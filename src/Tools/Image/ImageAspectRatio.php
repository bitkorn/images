<?php

namespace Bitkorn\Images\Tools\Image;

/**
 * Description of ImageAspectRatio
 *
 * @author Torsten Brieskorn
 */
class ImageAspectRatio
{

    /**
     * NOT READY IMPLEMENTED
     *
     * @param string $filePath
     * @param float $imageTargetWidth
     * @param float $imageTargetHeight
     * @param string $priority
     * @return int 0=OK; 1=width to low; 2=height to low
     * @throws \ImagickException
     * @throws \Exception
     */
    public static function checkImageSizeMin(string $filePath, float $imageTargetWidth, float $imageTargetHeight, $priority = 'width'): int
    {
        if (!file_exists($filePath)) {
            throw new \Exception('File ' . $filePath . ' does not exist.');
        }
        $imagick = new \Imagick($filePath);
        $geo = $imagick->getimagegeometry();
        $imageWidth = $geo['width'];
        $imageHeight = $geo['height'];
        if ($imageWidth < $imageTargetWidth) {
            return 1;
        }
        if ($imageHeight < $imageTargetHeight) {
            return 2;
        }
        $aspectRatioToOne = $imageTargetWidth / $imageTargetHeight;
        if ($priority == 'width') {

        }
    }

}

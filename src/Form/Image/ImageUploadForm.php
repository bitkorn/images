<?php

namespace Bitkorn\Images\Form\Image;

use Bitkorn\Trinket\Form\Element\Textarea;
use Laminas\Form\Element\File;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class ImageUploadForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface
{

    /**
     *
     * @var array
     */
    private $imagegroupIdAssoc;

    /**
     * @param array $imagegroupIdAssoc
     */
    public function setImagegroupIdAssoc(array $imagegroupIdAssoc): void
    {
        $this->imagegroupIdAssoc = $imagegroupIdAssoc;
    }

    function __construct($name = 'bk_images_image')
    {
        parent::__construct($name);
    }

    public function init()
    {
        $this->setAttribute('enctype', 'multipart/form-data');

        $imageTitle = new \Laminas\Form\Element\Text('bk_images_image_title');
        $imageTitle->setLabel('Bild Name');
        $imageTitle->setAttributes([
            'class' => 'w3-input',
            'title' => 'hieraus wird der SEO-freundliche Dateiname generiert!!!'
        ]);
        $this->add($imageTitle);

        $imageDesc = new Textarea('bk_images_image_desc');
        $imageDesc->setLabel('Bild Beschreibung');
        $imageDesc->setAttributes([
            'class' => 'w3-input w3-border',
            'title' => 'dieser Text erscheint zu dem Bild im Frontend'
        ]);
        $this->add($imageDesc);

        $imageDescIntern = new Textarea('bk_images_image_desc_intern');
        $imageDescIntern->setLabel('Bild Beschreibung Intern');
        $imageDescIntern->setAttributes([
            'class' => 'w3-input w3-border',
            'title' => 'dieser Text hilft dem User im Admin Bereich das Bild eindeutig zu identifizieren'
        ]);
        $this->add($imageDescIntern);

        $imagePriority = new Number('bk_images_image_priority');
        $imagePriority->setLabel('Priorität');
        $imagePriority->setAttributes([
            'class' => 'w3-input',
            'title' => 'Zur Sortierung im Frontend.'
        ]);
        $this->add($imagePriority);

        $imageGroup = new Select('bk_images_imagegroup_id');
        $imageGroup->setLabel('Gruppe');
        $imageGroup->setLabelAttributes(['title' => 'Warenwirtschaft']);
        $imageGroup->setAttributes([
            'class' => 'w3-input',
        ]);
        $imageGroup->setValueOptions(array_merge([0 => '-- bitte wählen --'], $this->imagegroupIdAssoc));
        $this->add($imageGroup);

        $this->add(array(
            'name' => 'bk_images_image_filename',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $image = new File('bk_images_image_image');
        $image->setAttributes([
            'class' => 'w3-input w3-border',
            'accept' => 'image/jpg,image/jpeg,image/png'
        ]);
        $this->add($image);

        $submit = new Submit('submit');
        $submit->setValue('upload & speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);

        parent::init();
    }

    public function getInputFilterSpecification()
    {
        return array(
            'bk_images_image_title' => array(
                'required' => true, // um einen Filename zu generieren
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ),
                    ),
                ),
            ),
            'bk_images_image_desc' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 32000,
                        ),
                    ),
                ),
            ),
            'bk_images_image_desc_intern' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                    array('name' => 'HtmlEntities'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ),
                    ),
                ),
            ),
            'bk_images_image_priority' => array(
                'required' => false, // um einen Filename zu generieren
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    ['name' => 'Digits']
                ),
            ),
            'bk_images_imagegroup_id' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->imagegroupIdAssoc),
                        ),
                    ),
                ),
            ),
            'bk_images_image_filename' => array(
                'required' => false, // generiert aus shop_article_image_title
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ),
                    ),
                ),
            ),
            'bk_images_image_image' => array(
                'required' => true,
                'filters' => array(
                ),
                'validators' => array(
                    [
                        'name' => 'filemimetype',
                        'break_chain_on_failure' => TRUE,
                        'options' => [
                            'mimeType' => 'image/jpeg, image/png',
                            'magicFile' => FALSE, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'messages' => array(
                                \Laminas\Validator\File\MimeType::FALSE_TYPE => "Falsches Dateiformat '%type%', nur jpeg/jpg | image/png!",
                            ),
                        ]
                    ],
//                        [
//                        'name' => 'fileimagesize',
//                        'options' => array(
//                            'maxWidth' => 1600, // 1600, 1440, 1280, 1120, 960
//                            'maxHeight' => 1600, // 900, 810, 720, 630, 540
//                            'messages' => array(
//                                \Laminas\Validator\File\ImageSize::HEIGHT_TOO_BIG => "Das Bild ist mit %height% zu hoch, max=%maxheight%!",
//                                \Laminas\Validator\File\ImageSize::WIDTH_TOO_BIG => "Das Bild ist mit %width% zu breit, max=%maxwidth%!",
//                            ),
//                        ),
//                    ],
                ),
            ),
        );
    }
}
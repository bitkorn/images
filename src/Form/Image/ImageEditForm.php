<?php

namespace Bitkorn\Images\Form\Image;

use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class ImageEditForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface
{

    /**
     *
     * @var array
     */
    private $imagegroupIdAssoc;

    function __construct($name = 'bk_images_image')
    {
        parent::__construct($name);
    }

    public function init()
    {

        $this->add(array(
            'name' => 'bk_images_image_id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $imageTitle = new \Laminas\Form\Element\Text('bk_images_image_title');
        $imageTitle->setLabel('Bild Name');
        $imageTitle->setAttributes([
            'class' => 'w3-input',
            'title' => 'hieraus wird der SEO-freundliche Dateiname generiert!!!'
        ]);
        $this->add($imageTitle);

        $imageDesc = new \Laminas\Form\Element\Textarea('bk_images_image_desc');
        $imageDesc->setLabel('Bild Beschreibung');
        $imageDesc->setAttributes([
            'class' => 'w3-input w3-border',
            'title' => 'dieser Text erscheint zu dem Bild im Frontend'
        ]);
        $this->add($imageDesc);

        $imagePriority = new \Laminas\Form\Element\Number('bk_images_image_priority');
        $imagePriority->setLabel('Priorität');
        $imagePriority->setAttributes([
            'class' => 'w3-input',
            'title' => 'Zur Sortierung im Frontend.'
        ]);
        $this->add($imagePriority);

        $imageGroup = new \Laminas\Form\Element\Select('bk_images_imagegroup_id');
        $imageGroup->setLabel('Gruppe');
        $imageGroup->setLabelAttributes(['title' => 'Warenwirtschaft']);
        $imageGroup->setAttributes([
            'class' => 'w3-input',
        ]);
        $imageGroup->setValueOptions(array_merge([0 => '-- bitte wählen --'], $this->imagegroupIdAssoc));
        $this->add($imageGroup);

        $submit = new \Laminas\Form\Element\Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);
    }

    public function getInputFilterSpecification()
    {
        return array(
            'bk_images_image_id' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Digits',
                    ),
                ),
            ),
            'bk_images_image_title' => array(
                'required' => true, // um einen Filename zu generieren
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ),
                    ),
                ),
            ),
            'bk_images_image_desc' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 32000,
                        ),
                    ),
                ),
            ),
            'bk_images_image_priority' => array(
                'required' => false, // um einen Filename zu generieren
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    ['name' => 'Digits']
                ),
            ),
            'bk_images_imagegroup_id' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'InArray',
                        'options' => array(
                            'haystack' => array_keys($this->imagegroupIdAssoc),
                        ),
                    ),
                ),
            ),
        );
    }

    /**
     * 
     * @param array $imagegroupIdAssoc
     */
    public function setImagegroupIdAssoc(array $imagegroupIdAssoc)
    {
        $this->imagegroupIdAssoc = $imagegroupIdAssoc;
    }

}
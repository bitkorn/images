<?php

namespace Bitkorn\Images\Factory\Controller\Admin;

use Bitkorn\Images\Controller\Admin\ImageAdminController;
use Bitkorn\Images\Form\Image\ImageEditForm;
use Bitkorn\Images\Form\Image\ImageUploadForm;
use Bitkorn\Images\Service\Image\ImageService;
use Bitkorn\Images\Table\Image\ImageTable;
use Bitkorn\Images\Tablex\Image\ImageTablex;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ImageAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ImageAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFolderTool($container->get(FolderTool::class));
        $controller->setImagesConfig($container->get('config')['bitkorn_images']);
        $controller->setImageService($container->get(ImageService::class));
        $controller->setImageTable($container->get(ImageTable::class));
        $controller->setImageTablex($container->get(ImageTablex::class));
        $controller->setImageUploadForm($container->get(ImageUploadForm::class));
        $controller->setImageEditForm($container->get(ImageEditForm::class));
        return $controller;
    }
}

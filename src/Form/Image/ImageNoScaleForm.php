<?php

namespace Bitkorn\Images\Form\Image;

use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class ImageNoScaleForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface
{

    /**
     *
     * @var array
     */
    private $imagegroupIdAssoc;

    /**
     *
     * @var bool
     */
    private $isEdit = false;

    /**
     * 
     * @param string $name
     */
    function __construct($name = 'bk_images_image_noscale')
    {
        parent::__construct($name);
    }

    public function init()
    {
        $this->setAttribute('enctype', 'multipart/form-data');

        if ($this->isEdit) {
            $this->add(array(
                'name' => 'bk_images_image_noscale_id',
                'attributes' => array(
                    'type' => 'hidden',
                ),
            ));
        }

        $imageTitle = new \Laminas\Form\Element\Text('bk_images_image_noscale_title');
        $imageTitle->setLabel('Bild Name');
        $imageTitle->setAttributes([
            'class' => 'w3-input',
            'title' => 'hieraus wird der SEO-freundliche Dateiname generiert!!!'
        ]);
        $this->add($imageTitle);

        $imageDesc = new \Laminas\Form\Element\Textarea('bk_images_image_noscale_desc');
        $imageDesc->setLabel('Bild Beschreibung');
        $imageDesc->setAttributes([
            'class' => 'w3-input w3-border',
            'title' => 'dieser Text erscheint zu dem Bild im Frontend'
        ]);
        $this->add($imageDesc);

        $imageDescIntern = new \Laminas\Form\Element\Textarea('bk_images_image_noscale_desc_intern');
        $imageDescIntern->setLabel('Bild Beschreibung Intern');
        $imageDescIntern->setAttributes([
            'class' => 'w3-input w3-border',
            'title' => 'dieser Text hilft dem User im Admin Bereich das Bild eindeutig zu identifizieren'
        ]);
        $this->add($imageDescIntern);

        $imagePriority = new \Laminas\Form\Element\Number('bk_images_image_noscale_priority');
        $imagePriority->setLabel('Priorität');
        $imagePriority->setAttributes([
            'class' => 'w3-input',
            'title' => 'Zur Sortierung im Frontend.'
        ]);
        $this->add($imagePriority);

        $imageGroup = new \Laminas\Form\Element\Select('bk_images_imagegroup_id');
        $imageGroup->setLabel('Gruppe');
        $imageGroup->setLabelAttributes(['title' => 'Warenwirtschaft']);
        $imageGroup->setAttributes([
            'class' => 'w3-input',
        ]);
        $imageGroup->setValueOptions(array_merge([0 => '-- bitte wählen --'], $this->imagegroupIdAssoc));
        $this->add($imageGroup);

        if (!$this->isEdit) {
            $this->add(array(
                'name' => 'bk_images_image_noscale_filename',
                'attributes' => array(
                    'type' => 'hidden',
                ),
            ));

            $image = new \Laminas\Form\Element\File('bk_images_image_noscale_image');
            $image->setAttributes([
                'class' => 'w3-input w3-border',
                'accept' => 'image/jpg,image/jpeg,image/png'
            ]);
            $this->add($image);

            $this->add(array(
                'name' => 'do_watermark',
                'type' => 'Laminas\Form\Element\Radio',
                'options' => array(
                    'label' => 'Do Watermark',
                    'value_options' => array(
                        '0' => 'no',
                        '1' => 'yes',
                    ),
                ), 'attributes' => array(
                    'title' => 'Places a watermark (image is defined in config) to the image.',
                    'value' => 0
                )
            ));
        }

        $submit = new \Laminas\Form\Element\Submit('submit');
        $submit->setValue('upload & speichern');
        $submit->setAttributes([
            'class' => 'w3-button w3-grey',
        ]);
        $this->add($submit);

        return parent::init();
    }

    public function getInputFilterSpecification()
    {
        $inputFilters = [];
        if ($this->isEdit) {
            $inputFilters['bk_images_image_noscale_id'] = array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Digits',
                    ),
                ),
            );
        }
        $inputFilters['bk_images_image_noscale_title'] = array(
            'required' => true, // um einen Filename zu generieren
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 200,
                    ),
                ),
            ),
        );

        $inputFilters['bk_images_image_noscale_desc'] = [
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'HtmlEntities'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 32000,
                    ),
                ),
        )];
        $inputFilters['bk_images_image_noscale_desc_intern'] = [
            'required' => false,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
                array('name' => 'HtmlEntities'),
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 1,
                        'max' => 200,
                    ),
                ),
        )];
        $inputFilters['bk_images_image_noscale_priority'] = [
            'required' => false, // um einen Filename zu generieren
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                ['name' => 'Digits']
        )];
        $inputFilters['bk_images_imagegroup_id'] = [
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'InArray',
                    'options' => array(
                        'haystack' => array_keys($this->imagegroupIdAssoc),
                    ),
                ),
        )];

        if (!$this->isEdit) {
            $inputFilters['bk_images_image_noscale_filename'] = [
                'required' => false, // generiert aus shop_article_image_noscale_title
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ),
                    ),
            )];
            $inputFilters['bk_images_image_noscale_image'] = [
                'required' => true,
                'filters' => array(),
                'validators' => array(
                    [
                        'name' => 'filemimetype',
                        'break_chain_on_failure' => TRUE,
                        'options' => [
                            'mimeType' => 'image/jpeg, image/png',
                            'magicFile' => FALSE, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                            'messages' => array(
                                \Laminas\Validator\File\MimeType::FALSE_TYPE => "Falsches Dateiformat '%type%', nur jpeg/jpg | image/png!",
                            ),
                        ]
                    ],
//                    [
//                        'name' => 'fileimagesize',
//                        'options' => array(
//                            'maxWidth' => 1600, // 1600, 1440, 1280, 1120, 960
//                            'maxHeight' => 1600, // 900, 810, 720, 630, 540
//                            'messages' => array(
//                                \Laminas\Validator\File\ImageSize::HEIGHT_TOO_BIG => "Das Bild ist mit %height% zu hoch, max=%maxheight%!",
//                                \Laminas\Validator\File\ImageSize::WIDTH_TOO_BIG => "Das Bild ist mit %width% zu breit, max=%maxwidth%!",
//                            ),
//                        ),
//                    ],
            )];
        $inputFilters['do_watermark'] = [
            'required' => true,
            'filters' => array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'InArray',
                    'options' => array(
                        'haystack' => [0, 1],
                    ),
                ),
        )];
        }

        return $inputFilters;
    }

    /**
     * 
     * @param array $imagegroupIdAssoc
     */
    public function setImagegroupIdAssoc(array $imagegroupIdAssoc)
    {
        $this->imagegroupIdAssoc = $imagegroupIdAssoc;
    }

    /**
     * 
     * @param bool $isEdit
     */
    public function setIsEdit(bool $isEdit)
    {
        $this->isEdit = $isEdit;
    }

}

?>

<?php

namespace Bitkorn\Images\View\Helper\Image;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;

/**
 * Description of ImageEmptySrc
 *
 * @author allapow
 */
class ImageEmptySrc extends AbstractViewHelper
{
    /**
     *
     * @var array
     */
    private $bitkornImagesConfig;

    /**
     * @param array $bitkornImagesConfig
     */
    public function setBitkornImagesConfig(array $bitkornImagesConfig)
    {
        $this->bitkornImagesConfig = $bitkornImagesConfig;
    }

    /**
     * @param array $imageScaling
     * @return string
     */
    public function __invoke(array $imageScaling)
    {
        if (count($imageScaling) != 1) {
            return '';
        }
        $imageScalingType = array_keys($imageScaling)[0];
        $imageScalingValue = $imageScaling[$imageScalingType];
        
        $imageSrcAbsolute = realpath($this->bitkornImagesConfig['image_path_absolute']) . '/noimage';
        if (!file_exists($imageSrcAbsolute)) {
            throw new \RuntimeException('ImagePath does not exist: ' . $imageSrcAbsolute);
        }
        $imageSrcRelative = $this->bitkornImagesConfig['image_path_relative'] . '/noimage';
        $imageDummyName = 'no_image';
        $imageFileName = '';
        switch ($imageScalingType) {
            case 'magicTwoSquare':
                $imageFileName = $imageDummyName . '_mts_' . $imageScalingValue . '.png';
                break;
            case 'fullHd':
                $imageFileName = $imageDummyName . '_fullhd.png';
                break;
            case 'sixteenToNine':
                $imageFileName = $imageDummyName . '_stn_' . $imageScalingValue . '.png';
                break;
            case 'custom':
                $imageFileName = $imageDummyName . '_' . $imageScalingValue . '.png';
                break;
            case 'original':
                $imageFileName = $imageDummyName . '.png';
                break;
        }
        if (empty($imageFileName) || !file_exists($imageSrcAbsolute . '/' . $imageFileName)) {
            throw new \RuntimeException('Image does not exist: ' . $imageSrcAbsolute . '/' . $imageFileName);
        }

        return $imageSrcRelative . '/' . $imageFileName;
    }
}

<?php

namespace Bitkorn\Images\Factory\Service\Image;

use Bitkorn\Images\Service\Image\ImageService;
use Bitkorn\Images\Table\Image\ImageGroupTable;
use Bitkorn\Images\Table\Image\ImageNoScaleTable;
use Bitkorn\Images\Table\Image\ImageTable;
use Bitkorn\Images\Table\Image\SliderTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ImageServiceFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$service = new ImageService();
		$service->setLogger($container->get('logger'));
        $service->setImagesConfig($container->get('config')['bitkorn_images']);
        $service->setImageTable($container->get(ImageTable::class));
        $service->setImageNoScaleTable($container->get(ImageNoScaleTable::class));
        $service->setImageGroupTable($container->get(ImageGroupTable::class));
        $service->setSliderTable($container->get(SliderTable::class));
		return $service;
	}
}

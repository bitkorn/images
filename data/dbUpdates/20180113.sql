/**
 * Author:  Torsten Brieskorn
 * Created: 13.01.2018
 */

ALTER TABLE systemgurt.bk_images_image ADD bk_images_image_desc_intern varchar(200) DEFAULT '' NOT NULL ;
ALTER TABLE systemgurt.bk_images_image CHANGE bk_images_image_desc_intern bk_images_image_desc_intern varchar(200) DEFAULT '' NOT NULL AFTER bk_images_image_desc ;
ALTER TABLE systemgurt.bk_images_image MODIFY COLUMN bk_images_image_title varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;

CREATE TABLE systemgurt.bk_images_image_noscale (
	bk_images_image_id int(10) unsigned NOT NULL auto_increment,
	bk_images_imagegroup_id int(10) unsigned NOT NULL,
	bk_images_image_title varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	bk_images_image_desc text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
	bk_images_image_desc_intern varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' NOT NULL,
	bk_images_image_filename tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	bk_images_image_extension varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	bk_images_image_priority int(11) DEFAULT 1 NOT NULL,
	bk_images_image_scaling text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'width and height separated with x',
	bk_images_image_time_create int(10) unsigned NOT NULL,
	CONSTRAINT `PRIMARY` PRIMARY KEY (bk_images_image_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci
COMMENT='' ;


ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_id bk_images_image_noscale_id int(10) unsigned NOT NULL auto_increment ;
ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_title bk_images_image_noscale_title varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_desc bk_images_image_noscale_desc text CHARACTER SET utf8 COLLATE utf8_general_ci NULL ;
ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_desc_intern bk_images_image_noscale_desc_intern varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' NOT NULL ;
ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_filename bk_images_image_noscale_filename tinytext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_extension bk_images_image_noscale_extension varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL ;
ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_priority bk_images_image_noscale_priority int(11) DEFAULT 1 NOT NULL ;
ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_scaling bk_images_image_noscale_scaling VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'width and height separated with x' ;
ALTER TABLE systemgurt.bk_images_image_noscale MODIFY COLUMN bk_images_image_noscale_scaling VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'width and height separated with x' ;
ALTER TABLE systemgurt.bk_images_image_noscale CHANGE bk_images_image_time_create bk_images_image_noscale_time_create int(10) unsigned NOT NULL ;

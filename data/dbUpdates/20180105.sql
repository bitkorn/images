/**
 * Author:  Torsten Brieskorn
 * Created: 05.01.2018
 */

CREATE TABLE systemgurt.bk_images_slider (
	bk_images_slider_id int(10) unsigned NOT NULL AUTO_INCREMENT,
	CONSTRAINT bk_images_slider_PK PRIMARY KEY (bk_images_slider_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci ;

ALTER TABLE systemgurt.bk_images_slider ADD bk_images_slider_desc text NOT NULL ;
ALTER TABLE systemgurt.bk_images_slider ADD bk_images_slider_filename TINYTEXT NOT NULL ;
ALTER TABLE systemgurt.bk_images_slider ADD bk_images_slider_extension varchar(10) NOT NULL ;
ALTER TABLE systemgurt.bk_images_slider ADD bk_images_slider_time_create int(10) unsigned NOT NULL ;

ALTER TABLE systemgurt.bk_images_slider ADD bk_images_slider_title TINYTEXT NOT NULL ;
ALTER TABLE systemgurt.bk_images_slider CHANGE bk_images_slider_title bk_images_slider_title TINYTEXT NOT NULL AFTER bk_images_slider_id ;

ALTER TABLE systemgurt.bk_images_slider ADD bk_images_slider_filename_thumb TINYTEXT NOT NULL ;
ALTER TABLE systemgurt.bk_images_slider CHANGE bk_images_slider_filename_thumb bk_images_slider_filename_thumb TINYTEXT NOT NULL AFTER bk_images_slider_filename ;

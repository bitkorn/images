<?php

namespace Bitkorn\Images\Tools\Image;

use Laminas\Log\Logger;

/**
 *
 * @author Torsten Brieskorn
 */
class ImageFilename
{

    /**
     *
     * @param string $shortSizeSpecification
     * @param Logger $logger
     * @return string
     */
    public static function shortToFilename(string $shortSizeSpecification, Logger $logger)
    {
        if (empty($shortSizeSpecification)) {
            return 'empty specs';
        }
        $specs = explode('_', $shortSizeSpecification);
//        $logger->debug(print_r($specs, true));
        switch ($specs[0]) {
            case 'original':
                return '';
            case 'mts':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                return '_mts_' . $specs[1];
            case 'fullHd':
                return '_fullhd';
            case 'stn':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                return '_stn_' . $specs[1];
            case 'custom':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                return '_custom_' . $specs[1];
        }
    }

    /**
     *
     * @param string $shortSizeSpecification
     * @param Logger $logger
     * @return array 0 => the short name for JSON sprecs (original, magicTwoSquare, fullHd, sixteenToNine, custom), 1 => one of the values from JSON ImageSpecs.
     * An empty Array on failure.
     */
    public static function shortToJsonArray(string $shortSizeSpecification, Logger $logger)
    {
        if (empty($shortSizeSpecification)) {
            return [];
        }
        $specs = explode('_', $shortSizeSpecification);
        $jsonSpecsArray = [];
//        $logger->debug(print_r($specs, true));
        switch ($specs[0]) {
            case 'original':
                $jsonSpecsArray[0] = 'original';
                $jsonSpecsArray[1] = 1;
                break;
            case 'mts':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                $jsonSpecsArray[0] = 'magicTwoSquare';
                $jsonSpecsArray[1] = intval($specs[1]);
                break;
            case 'fullHd':
                $jsonSpecsArray[0] = 'fullHd';
                $jsonSpecsArray[1] = 1;
                break;
            case 'stn':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                $jsonSpecsArray[0] = 'sixteenToNine';
                $jsonSpecsArray[1] = intval($specs[1]);
                break;
            case 'custom':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                $jsonSpecsArray[0] = 'custom';
                $jsonSpecsArray[1] = $specs[1];
                break;
        }
        return $jsonSpecsArray;
    }

    /**
     *
     * @param string $shortSizeSpecification
     * @param Logger $logger
     * @return string Human readable version of SizeSpecDef and 'original' if original.
     */
    public static function shortToSize(string $shortSizeSpecification, Logger $logger)
    {
        if (empty($shortSizeSpecification)) {
            return [];
        }
        $specs = explode('_', $shortSizeSpecification);
//        $logger->debug(print_r($specs, true));
        switch ($specs[0]) {
            case 'original':
                return 'original';
            case 'mts':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                return $specs[1] . ' x ' . $specs[1];
            case 'fullHd':
                return '1920 x 1080';
            case 'stn':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                return ($specs[1] * 16) . ' x ' . ($specs[1] * 9);
            case 'custom':
                if (empty($specs[1])) {
                    return 'empty [1]';
                }
                return str_replace('x', ' x ', $specs[1]);
        }
        return '';
    }

}

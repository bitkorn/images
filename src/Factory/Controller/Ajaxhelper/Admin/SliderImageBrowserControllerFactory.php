<?php

namespace Bitkorn\Images\Factory\Controller\Ajaxhelper\Admin;

use Bitkorn\Images\Controller\Ajaxhelper\Admin\SliderImageBrowserController;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SliderImageBrowserControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new SliderImageBrowserController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setImagesConfig($container->get('config')['bitkorn_images']);
        $controller->setSliderTable($container->get('BitkornImages\Table\Image\Slider'));
        return $controller;
    }
}

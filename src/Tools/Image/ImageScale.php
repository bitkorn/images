<?php

namespace Bitkorn\Images\Tools\Image;

/**
 * Description of ImageScale
 *
 * @author allapow
 */
class ImageScale
{

    private static $sixteenToNineDecimal = 1.7777777;

    /**
     *
     * @var array Key=multiplicator; value=[width, height]
     */
    private static $sixteenToNines = [
        1 => [16, 9],
        2 => [32, 18],
        4 => [64, 36], // thump XS
        6 => [96, 54],
        8 => [128, 72], // thump
        10 => [160, 90], // preview
        20 => [320, 180], // preview XL
        30 => [480, 270],
        40 => [640, 360],
        50 => [800, 450],
        60 => [960, 540],
        80 => [1280, 720],
        120 => [1920, 1080]
    ];
    private static $magicTwoSquares = [
        16 => [16, 16],
        32 => [32, 32],
        64 => [64, 64],
        128 => [128, 128],
        256 => [256, 256],
        512 => [512, 512],
        1024 => [1024, 1024]
    ];

    /**
     * 
     * @param string $fqfn Full qualified folder name ...without a ending slash.
     * @param string $imageName Image name without file-extension (only the nude name).
     * @param string $imageFormat
     * @param array $magicTwoSquares
     * @return array
     */
    public static function makeMagicTwoSquare($fqfn, $imageName, $imageFormat = 'jpg', array $magicTwoSquares = [])
    {
        if (empty($fqfn) || empty($imageName)) {
            throw new \RuntimeException('call ' . __CLASS__ . '->' . __FUNCTION__ . '() without imagepath or imagename.');
        }
        if (empty($magicTwoSquares)) {
            $magicTwoSquares = array_keys(self::$magicTwoSquares);
        }
        $biggestMagicTwoSquare = 0;
        foreach ($magicTwoSquares as $magicTwoSquare) {
            if (!array_key_exists($magicTwoSquare, self::$magicTwoSquares)) {
                throw new \RuntimeException('call ' . __CLASS__ . '->' . __FUNCTION__ . '() invalid $magicTwoSquares.');
            }
            if ($biggestMagicTwoSquare < $magicTwoSquare) {
                $biggestMagicTwoSquare = $magicTwoSquare;
            }
        }
        $imagick = new \Imagick($fqfn . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();
        if (($geo['width'] / $biggestMagicTwoSquare) < ($geo['height'] / $biggestMagicTwoSquare)) {
            $imagick->cropImage($geo['width'], floor($biggestMagicTwoSquare * $geo['width'] / $biggestMagicTwoSquare), 0,
                    (($geo['height'] - ($biggestMagicTwoSquare * $geo['width'] / $biggestMagicTwoSquare)) / 2));
        } else {
            $imagick->cropImage(ceil($biggestMagicTwoSquare * $geo['height'] / $biggestMagicTwoSquare), $geo['height'],
                    (($geo['width'] - ($biggestMagicTwoSquare * $geo['height'] / $biggestMagicTwoSquare)) / 2), 0);
        }

        rsort($magicTwoSquares); // biggest first
        $images = [];
        foreach ($magicTwoSquares as $magicTwoSquare) {
//            $imagick->thumbnailimage($magicTwoSquare, $magicTwoSquare, true);
            $imagick->scaleimage($magicTwoSquare, $magicTwoSquare, true);
            $imagick->setimageformat($imageFormat);
            if (file_put_contents($fqfn . '/' . $imageName . '_mts_' . $magicTwoSquare . '.' . $imageFormat, $imagick->getimageblob())) {
                $images[] = $magicTwoSquare;
            }
        }

        $imagick->clear();
        $imagick->destroy();
        return $images;
    }

    /**
     * Scale down to 1920x1080.
     * https://de.wikipedia.org/wiki/Full_HD
     * @param string $fullImagePath
     * @param string $imageName
     * @param string $imageFormat
     * @return string The filename.
     */
    public static function makeFullHd($fullImagePath, $imageName, $imageFormat = 'jpg')
    {
        $imagick = new \Imagick($fullImagePath . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();
        $widthFullHd = self::$sixteenToNines[120][0];
        $heigthFullHd = self::$sixteenToNines[120][1];
        if (($geo['width'] / $geo['height']) < self::$sixteenToNineDecimal) {
            $newHeight = floor($geo['width'] / 16 * 9);
            $imagick->cropImage($geo['width'], $newHeight, 0, (($geo['height'] - $newHeight) / 2));
        } else {
            $newWidth = floor($geo['height'] / 9 * 16);
            $imagick->cropImage($newWidth, $geo['height'], (($geo['width'] - $newWidth) / 2), 0);
//            $imagick->rotateimage(new \ImagickPixel('#00000000'), 90);
        }
        $geoSixteenToNine = $imagick->getimagegeometry();
        if ($geoSixteenToNine['width'] < $widthFullHd || $geoSixteenToNine['height'] < $heigthFullHd) {
//            return '';
        }
        $imagick->scaleimage($widthFullHd, $heigthFullHd, true);
        $imagick->setimageformat($imageFormat);
        if (file_put_contents($fullImagePath . '/' . $imageName . '_fullhd.' . $imageFormat, $imagick->getimageblob())) {
            $imagick->clear();
            $imagick->destroy();
            return 1;
        }
        $imagick->clear();
        $imagick->destroy();
        return 0;
    }

    /**
     * Scale down to some 16:9 with self::$sixteenToNines.
     * https://de.wikipedia.org/wiki/16:9
     * @param string $fullImagePath
     * @param string $imageName
     * @param string $imageFormat
     * @param array $sixteenToNineMultiplicators IndexKeys from self::$sixteenToNines. If empty then takes it all.
     * @return array Filenames, absolute folder, width and height. Indexed with $sixteenToNineMultiplicator.
     */
    public static function makeSixteenToNines($fullImagePath, $imageName, $imageFormat = 'jpg', array $sixteenToNineMultiplicators = [])
    {
//        $filenames = [];
        if (empty($sixteenToNineMultiplicators)) {
            $sixteenToNineMultiplicators = array_keys(self::$sixteenToNines);
        }
        $imagick = new \Imagick($fullImagePath . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();
        // make 16:9
        if (($geo['width'] / $geo['height']) <= self::$sixteenToNineDecimal) {
            $newHeight = floor($geo['width'] / 16 * 9);
            $imagick->cropImage($geo['width'], $newHeight, 0, (($geo['height'] - $newHeight) / 2));
        } else {
            $newWidth = floor($geo['height'] / 9 * 16);
            $imagick->cropImage($newWidth, $geo['height'], (($geo['width'] - $newWidth) / 2), 0);
//            $imagick->rotateimage(new \ImagickPixel('#00000000'), 90);
//            $imagick->setimagepage($imagick->getimagewidth(), $imagick->getimageheight(), 0, 0);
//            $imagick->setimageorientation(\Imagick::ORIENTATION_RIGHTTOP);
//            $geo = $imagick->getimagegeometry();
//            $newHeight = floor($geo['width'] / 16 * 9);
//            $imagick->cropImage($geo['width'], $newHeight, 0, (($geo['height'] - $newHeight) / 2));
        }
        krsort($sixteenToNineMultiplicators);
        $images = [];
        foreach ($sixteenToNineMultiplicators as $sixteenToNineMultiplicator) {
            if (!array_key_exists($sixteenToNineMultiplicator, self::$sixteenToNines)) {
                continue;
            }
            $width = self::$sixteenToNines[$sixteenToNineMultiplicator][0];
            $heigth = self::$sixteenToNines[$sixteenToNineMultiplicator][1];
            $imagick->scaleimage($width, $heigth, true);
            $imagick->setimageformat($imageFormat);
            file_put_contents($fullImagePath . '/' . $imageName . '_stn_' . $sixteenToNineMultiplicator . '.' . $imageFormat, $imagick->getimageblob());
//            $filenames[$sixteenToNineMultiplicator] = $imageFilename;
            $images[] = $sixteenToNineMultiplicator;
        }
        $imagick->clear();
        $imagick->destroy();
        return $images;
    }

    /**
     * 
     * @param string $fqfn
     * @param string $imageName
     * @param string $imageFormat
     * @param array $widthAndHeight ['width' => 27, 'height' => 42]
     * @return string
     * @throws \RuntimeException
     */
    public static function makeCustomScale(string $fqfn, string $imageName, $imageFormat = 'jpg', array $widthAndHeight = [])
    {
        if (empty($fqfn) || empty($imageName)) {
            throw new \RuntimeException('call ' . __CLASS__ . '->' . __FUNCTION__ . '() without imagepath or imagename.');
        }
        if (!isset($widthAndHeight['width']) || empty($widthAndHeight['width']) || !isset($widthAndHeight['height']) || empty($widthAndHeight['height'])) {
            return '';
        }

        $imagick = new \Imagick($fqfn . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();
        if (($geo['width'] / $widthAndHeight['width']) < ($geo['height'] / $widthAndHeight['height'])) {
            $imagick->cropImage($geo['width'], floor($widthAndHeight['height'] * $geo['width'] / $widthAndHeight['width']), 0,
                    (($geo['height'] - ($widthAndHeight['height'] * $geo['width'] / $widthAndHeight['width'])) / 2));
        } else {
            $imagick->cropImage(ceil($widthAndHeight['width'] * $geo['height'] / $widthAndHeight['height']), $geo['height'],
                    (($geo['width'] - ($widthAndHeight['width'] * $geo['height'] / $widthAndHeight['height'])) / 2), 0);
        }
        $custom = $widthAndHeight['width'] . 'x' . $widthAndHeight['height'];
//        $imagick->thumbnailimage($widthAndHeight['width'], $widthAndHeight['height'], true);
        $imagick->scaleimage($widthAndHeight['width'], $widthAndHeight['height'], true);
        $imagick->setimageformat($imageFormat);
        if (file_put_contents($fqfn . '/' . $imageName . '_' . $custom . '.' . $imageFormat, $imagick->getimageblob())) {
            $imagick->clear();
            $imagick->destroy();
            return $custom;
        }

        $imagick->clear();
        $imagick->destroy();
        return '';
    }

    /**
     * Scales an image and preserves the aspect ratio.
     * 
     * @param string $fqfn
     * @param string $imageName
     * @param float $customHeight
     * @param string $imageFormat
     * @return string
     * @throws \RuntimeException
     */
    public static function makeCustomHightScale(string $fqfn, string $imageName, float $customHeight, string $imageFormat = 'jpg'): string
    {
        if (empty($fqfn) || empty($imageName)) {
            throw new \RuntimeException('call ' . __CLASS__ . '->' . __FUNCTION__ . '() without imagepath or imagename.');
        }
        if (empty($customHeight)) {
            return '';
        }

        $imagick = new \Imagick($fqfn . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();
        $widthNew = ($geo['width'] / $geo['height']) * $customHeight; // oldwidth / oldheight = newwidth / newheight
        $imagick->scaleimage($widthNew, $customHeight, true);
        $imagick->setimageformat($imageFormat);
        if (file_put_contents($fqfn . '/' . $imageName . '_' . $customHeight . 'h.' . $imageFormat, $imagick->getimageblob())) {
            $imagick->clear();
            $imagick->destroy();
            return $customHeight . 'h';
        }

        $imagick->clear();
        $imagick->destroy();
        return '';
    }

    /**
     * 
     * @param string $fqfn
     * @param string $imageName
     * @param string $imageFormat
     * @return array
     * @throws \RuntimeException
     */
    public static function getImageGeometry(string $fqfn, string $imageName, string $imageFormat = 'jpg'): array
    {
        if (empty($fqfn) || empty($imageName)) {
            throw new \RuntimeException('call ' . __CLASS__ . '->' . __FUNCTION__ . '() without imagepath or imagename.');
        }

        $imagick = new \Imagick($fqfn . '/' . $imageName . '.' . $imageFormat);
        $geo = $imagick->getimagegeometry();

        $imagick->clear();
        $imagick->destroy();
        return $geo;
    }

    public static function getSixteenToNines()
    {
        return self::$sixteenToNines;
    }

    public static function getMagicTwoSquares()
    {
        return self::$magicTwoSquares;
    }

}

<?php

namespace Bitkorn\Images\Controller\Ajaxhelper\Admin;

use Bitkorn\Images\Table\Image\ImageGroupTable;
use Bitkorn\Images\Table\Image\ImageTable;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;
use Bitkorn\Images\Tools\Image\ImageFilename as ImageFilenameTools;

class ImageBrowserController extends AbstractUserController
{

    protected ImageTable $imageTable;
    protected ImageGroupTable $imageGroupTable;

    public function setImageTable(ImageTable $imageTable): void
    {
        $this->imageTable = $imageTable;
    }

    public function setImageGroupTable(ImageGroupTable $imageGroupTable)
    {
        $this->imageGroupTable = $imageGroupTable;
    }

    /**
     * @return ViewModel|Response
     */
    public function imageBrowseAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/nothing');

        $page = (int)$this->params()->fromQuery('page', 0);
        $itemsPerPage = (int)$this->params()->fromQuery('itemsperpage', 2000);
        $offset = $page * $itemsPerPage;
        $imageGroupCategory = (int)$this->params()->fromQuery('category', 2);

        $images = $this->imageTable->getImagesByImageGroupId($imageGroupCategory, $itemsPerPage, $offset);
        $viewModel->setVariable('images', $images);

        $viewModel->setVariable('imagesRelRoot', '/img/bkimages/');

        return $viewModel;
    }

    /**
     * @return Response|ViewModel
     */
    public function imageBrowseSizeAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/nothing');
        $imageSizedef = $this->params('image_sizedef');
        if (!$imageSizedef) {
            return $viewModel;
        }
        $imageSizedefFile = ImageFilenameTools::shortToFilename($imageSizedef, $this->logger);
        $viewModel->setVariable('imageSizedefFile', $imageSizedefFile);
        $jsonSpecsArray = ImageFilenameTools::shortToJsonArray($imageSizedef, $this->logger);
        if (count($jsonSpecsArray) != 2) {
            throw new \RuntimeException('Empty shortToJsonArray');
        }
        $viewModel->setVariable('imageSizeJsonSpecsArray', $jsonSpecsArray);
        $viewModel->setVariable('imageSizeReadable', ImageFilenameTools::shortToSize($imageSizedef, $this->logger));

        $page = (int)$this->params()->fromQuery('page', 0);
        $itemsPerPage = (int)$this->params()->fromQuery('itemsperpage', 2000);
        $offset = $page * $itemsPerPage;
        $imageGroupCategory = (int)$this->params()->fromQuery('category', 2);

        $images = $this->imageTable->getImagesByImageGroupId($imageGroupCategory, $itemsPerPage, $offset);
        $viewModel->setVariable('images', $images);

        $viewModel->setVariable('imagesRelRoot', '/img/bkimages/');

        return $viewModel;
    }

}

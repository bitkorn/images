<?php

namespace Bitkorn\Images\View\Helper\Slider;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;

/**
 *
 * @author allapow
 */
class SliderImageSrc extends AbstractViewHelper
{

    /**
     *
     * @var array
     */
    private $bitkornImagesConfig;

    /**
     * @param array $bitkornImagesConfig
     */
    public function setBitkornImagesConfig(array $bitkornImagesConfig)
    {
        $this->bitkornImagesConfig = $bitkornImagesConfig;
    }

    /**
     * @param array $sliderImageData
     * @param bool $thumb
     * @return string
     */
    public function __invoke(array $sliderImageData, $thumb = false)
    {
        if (empty($sliderImageData)) {
            return '';
        }
        $sliderPathAbs = realpath($this->bitkornImagesConfig['slider_path_absolute'] . '/'
                . date('Y', $sliderImageData['bk_images_slider_time_create']) . '/'
                . date('m', $sliderImageData['bk_images_slider_time_create']) . '/'
                . $sliderImageData['bk_images_slider_id']) . '/';
        $sliderPathRel = $this->bitkornImagesConfig['slider_path_relative'] . '/'
            . date('Y', $sliderImageData['bk_images_slider_time_create']) . '/'
            . date('m', $sliderImageData['bk_images_slider_time_create']) . '/'
            . $sliderImageData['bk_images_slider_id'] . '/';
        if ($thumb) {
            $abs = $sliderPathAbs . $sliderImageData['bk_images_slider_filename_thumb'] . '.' . $sliderImageData['bk_images_slider_extension'];
            if (!file_exists($abs)) {
                $this->logger->err($abs);
                throw new \RuntimeException('Image does not exist: ' . $abs);
            }
            return $sliderPathRel . $sliderImageData['bk_images_slider_filename_thumb'] . '.' . $sliderImageData['bk_images_slider_extension'];
        } else {
            $abs = $sliderPathAbs . '/' . $sliderImageData['bk_images_slider_filename'] . '.' . $sliderImageData['bk_images_slider_extension'];
            if (!file_exists($abs)) {
                throw new \RuntimeException('Image does not exist: ' . $abs);
            }
            return $sliderPathRel . $sliderImageData['bk_images_slider_filename'] . '.' . $sliderImageData['bk_images_slider_extension'];
        }
    }

}

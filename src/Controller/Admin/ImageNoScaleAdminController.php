<?php

namespace Bitkorn\Images\Controller\Admin;

use Bitkorn\Images\Form\Image\ImageNoScaleForm;
use Bitkorn\Images\Table\Image\ImageNoScaleTable;
use Bitkorn\Images\Tools\Image\ImageScale;
use Bitkorn\Images\Tools\Image\ImageWatermark;
use Bitkorn\Trinket\Observer\ObserverManager;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\Tools\Filesystem\FilenameTool;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

/**
 * @author allapow
 */
class ImageNoScaleAdminController extends AbstractUserController
{
    protected FolderTool $folderTool;
    protected array $imagesConfig;
    protected ImageNoScaleTable $imageNoScaleTable;
    protected ImageNoScaleForm $imageNoScaleForm;

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    public function setImagesConfig(array $imagesConfig): void
    {
        $this->imagesConfig = $imagesConfig;
    }

    public function setImageNoScaleTable(ImageNoScaleTable $imageNoScaleTable)
    {
        $this->imageNoScaleTable = $imageNoScaleTable;
    }

    public function setImageNoScaleForm(ImageNoScaleForm $imageNoScaleForm)
    {
        $this->imageNoScaleForm = $imageNoScaleForm;
    }

    /**
     * @return ViewModel|Response
     */
    public function imagesNoScaleAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        $images = $this->imageNoScaleTable->getImages();
        $viewModel->setVariable('images', $images);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['imageDelete']['imageId'])) {
                $jsonModel = new JsonModel();
                $deleteStatus = 0;
                $imageId = (int)$postData['imageDelete']['imageId'];
                ObserverManager::getInstance()->trigger('bitkornimages.imagenoscale.delete.before', ['imageId' => $imageId]);
                $image = $this->imageNoScaleTable->getImageById($imageId);
                if (!empty($image) && $this->imageNoScaleTable->delete(['bk_images_image_noscale_id' => $imageId])) {
                    $folder = $this->folderTool->computeDateIdFolder($this->imagesConfig['image_path_absolute_no_scale'],
                        date('Y', $image['bk_images_image_noscale_time_create']), date('m', $image['bk_images_image_noscale_time_create']), $imageId);

                    if ($this->folderTool->deleteDirRecursive($folder) === true) {
                        $deleteStatus = 1;
                    }
                }
                $jsonModel->setVariable('status', $deleteStatus);
                return $jsonModel;
            }
        }

        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function uploadImageNoScaleAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        $this->imageNoScaleForm->init();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            $this->imageNoScaleForm->setData($postData);
            if ($this->imageNoScaleForm->isValid()) {
                $formData = $this->imageNoScaleForm->getData();
                $fileinfo = new \finfo(FILEINFO_MIME);
                $extension01 = explode(';', $fileinfo->file($formData['bk_images_image_noscale_image']['tmp_name']));
                $extension02 = explode('/', $extension01[0]);
                $extension = $extension02[1];
                if (empty($extension)) {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text'  => 'Die Datei ist defekt. Bitte benutze eine andere oder repariere diese.'
                    ];
                } else {
                    $currentTime = time();
                    $newImageId = $this->imageNoScaleTable->saveImage([
                        'bk_images_imagegroup_id'             => $formData['bk_images_imagegroup_id'],
                        'bk_images_image_noscale_title'       => $formData['bk_images_image_noscale_title'],
                        'bk_images_image_noscale_desc'        => $formData['bk_images_image_noscale_desc'],
                        'bk_images_image_noscale_desc_intern' => $formData['bk_images_image_noscale_desc_intern'],
                        'bk_images_image_noscale_filename'    => '',
                        'bk_images_image_noscale_extension'   => $extension,
                        'bk_images_image_noscale_priority'    => (int)$formData['bk_images_image_noscale_priority'],
                        'bk_images_image_noscale_scaling'     => '',
                        'bk_images_image_noscale_time_create' => $currentTime
                    ]);
                    if ($newImageId > 0) {
                        $folder = $this->folderTool->computeDateIdFolder($this->imagesConfig['image_path_absolute_no_scale'],
                            date('Y', $currentTime), date('m', $currentTime), $newImageId);
                        $filename = FilenameTool::computeSeoFriendlyFilename($formData['bk_images_image_noscale_title']);
                        $filenameCompl = $filename . '.' . $extension;
                        if ($formData['do_watermark'] == 1) {
                            ImageWatermark::doWatermark($formData['bk_images_image_noscale_image']['tmp_name']);
                        }
                        if (move_uploaded_file($formData['bk_images_image_noscale_image']['tmp_name'], $folder . '/' . $filenameCompl)) {
                            $imageGeo = ImageScale::getImageGeometry($folder, $filename, $extension);
                            if (!empty($imageGeo) && $this->imageNoScaleTable->updateImage([
                                    'bk_images_image_noscale_filename' => $filename,
                                    'bk_images_image_noscale_scaling'  => $imageGeo['width'] . 'x' . $imageGeo['height']
                                ], $newImageId) < 0) {
                                $this->layout()->message = [
                                    'level' => 'error',
                                    'text'  => 'Es gab einen Fehler mit der Datenbank.'
                                ];
                            } else {
                                $this->layout()->message = [
                                    'level' => 'success',
                                    'text'  => 'Das Bild wurde mit seinen Daten gespeichert.'
                                ];
                            }
                        } else {
                            $this->imageNoScaleTable->delete(['bk_images_image_noscale_id' => $newImageId]);
                            $this->layout()->message = [
                                'level' => 'error',
                                'text'  => 'Es gab einen Fehler mit dem Dateisystem.'
                            ];
                        }
                        $this->imageNoScaleForm->init();
                    } else {
                        $this->layout()->message = [
                            'level' => 'error',
                            'text'  => 'Es gab einen Fehler mit der Datenbank.'
                        ];
                    }
                }
            }
        }

        $viewModel->setVariable('imageNoScaleForm', $this->imageNoScaleForm);

        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     *
     * @return ViewModel|Response
     */
    public function editImageNoScaleAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $imageId = $this->params('image_id');
        $imageData = $this->imageNoScaleTable->getImageById($imageId);
        if (empty($imageData)) {
            return $this->redirect()->toRoute('bitkornimages_admin_imagesnoscale');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('imageId', $imageId);
        $this->imageNoScaleForm->setIsEdit(true);
        $this->imageNoScaleForm->init();
        $this->imageNoScaleForm->setData($imageData);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $this->imageNoScaleForm->setData($postData);
            if ($this->imageNoScaleForm->isValid()) {
                $formData = $this->imageNoScaleForm->getData();
                unset($formData['submit']);
                if ($this->imageNoScaleTable->updateImage($formData, $imageId) >= 0) {
                    $this->layout()->message = [
                        'level' => 'success',
                        'text'  => 'Die Daten wurden gespeichert.'
                    ];
                } else {
                    $this->layout()->message = [
                        'level' => 'error',
                        'text'  => 'Es gab einen Fehler mit der Datenbank.'
                    ];
                }
            }
        }

        $viewModel->setVariable('imageEditForm', $this->imageNoScaleForm);

        $this->layout('layout/admin');
        return $viewModel;
    }

}

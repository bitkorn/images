create table bk_images_imagegroup
(
    bk_images_imagegroup_id serial not null
        constraint bk_images_imagegroup_pk
            primary key,
    bk_images_imagegroup_name varchar(100) not null,
    bk_images_imagegroup_priority integer default 1 not null
);

alter table bk_images_imagegroup owner to postgres;

create table bk_images_image
(
    bk_images_image_id serial not null
        constraint bk_images_image_pk
            primary key,
    bk_images_imagegroup_id integer not null
        constraint bk_images_image_bk_images_imagegroup_bk_images_imagegroup_id_fk
            references bk_images_imagegroup,
    bk_images_image_title varchar(200) not null,
    bk_images_image_desc text not null,
    bk_images_image_desc_intern varchar(200) default ''::character varying not null,
    bk_images_image_filename varchar(200) not null,
    bk_images_image_extension varchar(10) not null,
    bk_images_image_priority integer default 1 not null,
    bk_images_image_scaling text not null,
    bk_images_image_time_create integer not null
);

comment on column bk_images_image.bk_images_image_scaling is 'JSON {"magicTwoSquare":[1024,32,16],"fullHd":1,"sixteenToNine":[60,50,40,30,20,10,8,6],"custom":["42x27"]}';

alter table bk_images_image owner to postgres;

create table bk_images_image_noscale
(
    bk_images_image_noscale_id serial not null
        constraint bk_images_image_noscale_pk
            primary key,
    bk_images_imagegroup_id integer not null
        constraint bk_images_image_noscale_bk_images_imagegroup_bk_images_imagegro
            references bk_images_imagegroup,
    bk_images_image_noscale_title varchar(200) not null,
    bk_images_image_noscale_desc text,
    bk_images_image_noscale_desc_intern varchar(200) default ''::character varying not null,
    bk_images_image_noscale_filename varchar(200) not null,
    bk_images_image_noscale_extension varchar(10) not null,
    bk_images_image_noscale_priority integer default 1 not null,
    bk_images_image_noscale_scaling varchar(100) not null,
    bk_images_image_noscale_time_create integer not null
);

alter table bk_images_image_noscale owner to postgres;

create table bk_images_slider
(
    bk_images_slider_id serial not null
        constraint bk_images_slider_pk
            primary key,
    bk_images_slider_title varchar(200) not null,
    bk_images_slider_desc text not null,
    bk_images_slider_filename varchar(200) not null,
    bk_images_slider_filename_thumb varchar(200) not null,
    bk_images_slider_extension varchar(10) not null,
    bk_images_slider_time_create integer not null
);

alter table bk_images_slider owner to postgres;


<?php

namespace Bitkorn\Images\Table\Image;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ImageGroupTable extends AbstractLibTable
{

    /**
     *
     * @var string
     */
    protected $table = 'bk_images_imagegroup';

    public function getImageGroupById($id)
    {
        $select = $this->sql->select();
        $select->where(array(
            'bk_images_imagegroup_id' => $id,
        ));
        /** @var HydratingResultSet $result */
        $result = $this->selectWith($select);
        if ($result->valid() && $result->count() == 1) {
            return $result->current()->getArrayCopy();
        }
        return [];
    }

    public function saveImageGroup(array $data)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($data);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }

        return $this->executeInsert($insert);
    }

    public function getImageGroupIdAssoc()
    {
        $select = $this->sql->select();
        $idAssoc = [];
        try {
            $select->order('bk_images_imagegroup_priority DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $resultArr = $result->toArray();
                foreach ($resultArr as $res) {
                    $idAssoc[$res['bk_images_imagegroup_id']] = $res['bk_images_imagegroup_name'];
                }
            }
        } catch (\Laminas\Db\Adapter\Exception\RuntimeException $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return $idAssoc;
    }

}
<?php

namespace Bitkorn\Images\Factory\Form\Image;

use Bitkorn\Images\Form\Image\ImageUploadForm;
use Bitkorn\Images\Table\Image\ImageGroupTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ImageUploadFormFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$form = new ImageUploadForm();
		/** @var ImageGroupTable $imageGroupTable */
		$imageGroupTable = $container->get(ImageGroupTable::class);
		$form->setImagegroupIdAssoc($imageGroupTable->getImageGroupIdAssoc());
		return $form;
	}
}

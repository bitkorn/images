<?php

namespace Bitkorn\Images\Factory\Controller\Admin;

use Bitkorn\Images\Controller\Admin\SliderImageAdminController;
use Bitkorn\Images\Form\Slider\SliderEditForm;
use Bitkorn\Images\Form\Slider\SliderUploadForm;
use Bitkorn\Images\Table\Image\SliderTable;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SliderImageAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new SliderImageAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFolderTool($container->get(FolderTool::class));
        $controller->setImagesConfig($container->get('config')['bitkorn_images']);
        $controller->setSliderTable($container->get(SliderTable::class));
        $controller->setSliderUploadForm($container->get(SliderUploadForm::class));
        $controller->setSliderEditForm($container->get(SliderEditForm::class));
        return $controller;
    }
}

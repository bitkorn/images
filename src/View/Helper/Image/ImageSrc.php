<?php

namespace Bitkorn\Images\View\Helper\Image;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Trinket\View\Helper\AbstractViewHelper;

/**
 *
 * @author allapow
 */
class ImageSrc extends AbstractViewHelper
{

    /**
     *
     * @var array
     */
    private $bitkornImagesConfig;

    /**
     * @param array $bitkornImagesConfig
     */
    public function setBitkornImagesConfig(array $bitkornImagesConfig)
    {
        $this->bitkornImagesConfig = $bitkornImagesConfig;
    }

    /**
     * @param $imageId int bk_images_image_id
     * @param array $imageScaling E.g. `['sixteenToNine' => 10]`
     * @param ImagesEntity $imagesEntity
     * @return string
     */
    public function __invoke($imageId, array $imageScaling, ImagesEntity $imagesEntity)
    {
        if (empty($imageId) || count($imageScaling) != 1 || empty($imagesEntity)) {
            return '';
        }
        $imageScalingArray = json_decode($imagesEntity->getImageScaling($imageId), true);
        if (!$imageScalingArray) {
            throw new \RuntimeException('ImageScaling failure. ImageScaling: ' . $imagesEntity->getImageScaling($imageId));
        }
        $imageScalingType = array_keys($imageScaling)[0];
        $imageScalingValue = $imageScaling[$imageScalingType];
        if (!isset($imageScalingArray[$imageScalingType]) || empty($imageScalingArray[$imageScalingType])) {
            return '';
        }
        $imageYear = date('Y', $imagesEntity->getImageTimeCreate($imageId));
        $imageMonth = date('m', $imagesEntity->getImageTimeCreate($imageId));
        $imageSrcAbsolute = realpath($this->bitkornImagesConfig['image_path_absolute']) . '/' . $imageYear . '/' . $imageMonth . '/' . $imageId;
        if (!file_exists($imageSrcAbsolute)) {
            throw new \RuntimeException('ImagePath does not exist: ' . $imageSrcAbsolute);
        }
        $imageSrcRelative = $this->bitkornImagesConfig['image_path_relative'] . '/' . $imageYear . '/' . $imageMonth . '/' . $imageId;
        $imageFileName = '';
        switch ($imageScalingType) {
            case 'magicTwoSquare':
                $imageFileName = $imagesEntity->getImageFilename($imageId) . '_mts_' . $imageScalingValue . '.' . $imagesEntity->getImageExtension($imageId);
                break;
            case 'fullHd':
                $imageFileName = $imagesEntity->getImageFilename($imageId) . '_fullhd.' . $imagesEntity->getImageExtension($imageId);
                break;
            case 'sixteenToNine':
                $imageFileName = $imagesEntity->getImageFilename($imageId) . '_stn_' . $imageScalingValue . '.' . $imagesEntity->getImageExtension($imageId);
                break;
            case 'custom':
                $imageFileName = $imagesEntity->getImageFilename($imageId) . '_' . $imageScalingValue . '.' . $imagesEntity->getImageExtension($imageId);
                break;
            case 'original':
                $imageFileName = $imagesEntity->getImageFilename($imageId) . '.' . $imagesEntity->getImageExtension($imageId);
                break;
        }
        if (empty($imageFileName) || !file_exists($imageSrcAbsolute . '/' . $imageFileName)) {
            throw new \RuntimeException('Image does not exist: ' . $imageSrcAbsolute . '/' . $imageFileName);
        }

        return $imageSrcRelative . '/' . $imageFileName;
    }

}

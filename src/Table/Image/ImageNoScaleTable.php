<?php

namespace Bitkorn\Images\Table\Image;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class ImageNoScaleTable extends AbstractLibTable
{

    /**
     *
     * @var string
     */
    protected $table = 'bk_images_image_noscale';

    /**
     * 
     * @param array $storage
     * @return int
     */
    public function saveImage(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            $this->insertWith($insert);
            $newId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.bk_images_image_noscale_bk_images_image_noscale_id_seq');
            if (empty($newId)) {
                return -1;
            }
            return $newId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * 
     * @param int $imageId
     * @return array
     */
    public function getImageById($imageId)
    {
        $select = $this->sql->select();
        try {
            $select->where(array(
                'bk_images_image_noscale_id' => $imageId,
            ));
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getImages()
    {
        $select = $this->sql->select();
        try {
            $select->order('bk_images_image_noscale_priority DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getImagesByIds(array $imageIds)
    {
        $select = $this->sql->select();
        try {
            $select->where->in('bk_images_image_noscale_id', $imageIds);
            $select->order('bk_images_image_noscale_priority DESC');
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateImage($imageData, $imageId)
    {
        if(empty($imageId)) {
            return -1;
        }
        unset($imageData['bk_images_image_noscale_id']);
        $update = $this->sql->update();
        try {
            $update->set($imageData);
            $update->where(['bk_images_image_noscale_id' => $imageId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateImageScale($imageScale, $imageId)
    {
        if(empty($imageId)) {
            return -1;
        }
        $update = $this->sql->update();
        try {
            $update->set(['bk_images_image_noscale_scaling' => $imageScale]);
            $update->where(['bk_images_image_noscale_id' => $imageId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param int $imageGroupId
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getImagesByImageGroupId(int $imageGroupId, int $limit, int $offset)
    {
        $select = $this->sql->select();
        try {
            $select->where(['bk_images_imagegroup_id' => $imageGroupId]);
            $select->order('bk_images_image_noscale_priority DESC');
            $select->limit($limit);
            $select->offset($offset);
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}

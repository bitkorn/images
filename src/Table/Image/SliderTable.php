<?php

namespace Bitkorn\Images\Table\Image;

use Bitkorn\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;

/**
 *
 * @author allapow
 */
class SliderTable extends AbstractLibTable
{

    /**
     *
     * @var string
     */
    protected $table = 'bk_images_slider';

    /**
     *
     * @param array $storage
     * @return int
     */
    public function saveSlider(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            $this->insertWith($insert);
            $newId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.bk_images_slider_bk_images_slider_id_seq');
            if (empty($newId)) {
                return -1;
            }
            return $newId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     *
     * @param int $imageId
     * @return array
     */
    public function getSliderById($imageId)
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'bk_images_slider_id' => $imageId,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getSliders()
    {
        $select = $this->sql->select();
        try {
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function updateSliderFilename(string $sliderFilename, string $sliderFilenameThumb, int $imageId): int
    {
        if (empty($imageId)) {
            return -1;
        }
        $update = $this->sql->update();
        try {
            $update->set(['bk_images_slider_filename' => $sliderFilename, 'bk_images_slider_filename_thumb' => $sliderFilenameThumb]);
            $update->where(['bk_images_slider_id' => $imageId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateSliderData(int $sliderImageId, array $sliderData): int
    {
        $update = $this->sql->update();
        try {
            $update->set($sliderData);
            $update->where(['bk_images_slider_id' => $sliderImageId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

}

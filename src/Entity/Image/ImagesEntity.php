<?php

namespace Bitkorn\Images\Entity\Image;

/**
 * Description of ImageEntity
 *
 * @author allapow
 */
class ImagesEntity
{
    
    /**
     *
     * @var array Array with Key=property; value=db column
     */
    protected $mapping = [
        'bk_images_image_id' => 'bk_images_image_id',
        'bk_images_imagegroup_id' => 'bk_images_imagegroup_id',
        'bk_images_image_title' => 'bk_images_image_title',
        'bk_images_image_desc' => 'bk_images_image_desc',
        'bk_images_image_filename' => 'bk_images_image_filename',
        'bk_images_image_extension' => 'bk_images_image_extension',
        'bk_images_image_priority' => 'bk_images_image_priority',
        'bk_images_image_scaling' => 'bk_images_image_scaling',
        'bk_images_image_time_create' => 'bk_images_image_time_create',
    ];

    /**
     *
     * @var array 
     */
    protected $storageItems = [];
            
    /**
     * Use this function and NOT exchangeArray($data) from \Bitkorn\Images\Entity\AbstractEntity()!
     * @param array $imagesData
     * @return boolean
     */
    public function exchangeArrayImages(array $imagesData)
    {
        foreach ($imagesData as $imageRow) {
            $tmp = [];
            $storageKeys = array_values($this->mapping);
            foreach ($storageKeys as $storageKey) {
                $tmp[$storageKey] = '';
            }
            if (!is_array($imageRow)) {
                continue;
            }
            foreach ($imageRow as $key => $value) {
                if (isset($this->mapping[$key])) {
                    $tmp[$this->mapping[$key]] = $value;
                }
            }
            $this->storageItems[$imageRow['bk_images_image_id']] = $tmp;
        }
        if (empty($this->storageItems)) {
            return false;
        }
        return true;
    }
    
    /**
     * 
     * @param int $imageId
     * @return string
     */
    public function getImageTitle($imageId)
    {
        if(!isset($this->storageItems[$imageId]['bk_images_image_title'])) {
            return '';
        }
        return $this->storageItems[$imageId]['bk_images_image_title'];
    }
    
    /**
     * 
     * @param int $imageId
     * @return string
     */
    public function getImageDesc($imageId)
    {
        if(!isset($this->storageItems[$imageId]['bk_images_image_desc'])) {
            return '';
        }
        return $this->storageItems[$imageId]['bk_images_image_desc'];
    }
    
    /**
     * 
     * @param int $imageId
     * @return string
     */
    public function getImageFilename($imageId)
    {
        if(!isset($this->storageItems[$imageId]['bk_images_image_filename'])) {
            return '';
        }
        return $this->storageItems[$imageId]['bk_images_image_filename'];
    }
    
    /**
     * 
     * @param int $imageId
     * @return string
     */
    public function getImageExtension($imageId)
    {
        if(!isset($this->storageItems[$imageId]['bk_images_image_extension'])) {
            return '';
        }
        return $this->storageItems[$imageId]['bk_images_image_extension'];
    }
    
    /**
     * 
     * @param int $imageId
     * @return string
     */
    public function getImageScaling($imageId)
    {
        if(!isset($this->storageItems[$imageId]['bk_images_image_scaling'])) {
            return '';
        }
        return $this->storageItems[$imageId]['bk_images_image_scaling'];
    }
    
    /**
     * 
     * @param int $imageId
     * @return int
     */
    public function getImageTimeCreate($imageId)
    {
        if(!isset($this->storageItems[$imageId]['bk_images_image_time_create'])) {
            return '';
        }
        return (int) $this->storageItems[$imageId]['bk_images_image_time_create'];
    }
}

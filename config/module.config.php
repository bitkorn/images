<?php

namespace Bitkorn\Images;

use Bitkorn\Images\Controller\Admin\ImageAdminController;
use Bitkorn\Images\Controller\Admin\ImageNoScaleAdminController;
use Bitkorn\Images\Controller\Admin\SliderImageAdminController;
use Bitkorn\Images\Controller\Ajaxhelper\Admin\ImageBrowserController;
use Bitkorn\Images\Controller\Ajaxhelper\Admin\SliderImageBrowserController;
use Bitkorn\Images\Factory\Controller\Admin\ImageAdminControllerFactory;
use Bitkorn\Images\Factory\Controller\Admin\ImageNoScaleAdminControllerFactory;
use Bitkorn\Images\Factory\Controller\Admin\SliderImageAdminControllerFactory;
use Bitkorn\Images\Factory\Controller\Ajaxhelper\Admin\ImageBrowserControllerFactory;
use Bitkorn\Images\Factory\Controller\Ajaxhelper\Admin\SliderImageBrowserControllerFactory;
use Bitkorn\Images\Factory\Form\Image\ImageEditFormFactory;
use Bitkorn\Images\Factory\Form\Image\ImageNoScaleFormFactory;
use Bitkorn\Images\Factory\Form\Image\ImageUploadFormFactory;
use Bitkorn\Images\Factory\Form\Slider\SliderEditFormFactory;
use Bitkorn\Images\Factory\Form\Slider\SliderUploadFormFactory;
use Bitkorn\Images\Factory\Service\Image\ImageServiceFactory;
use Bitkorn\Images\Factory\Table\Image\ImageGroupTableFactory;
use Bitkorn\Images\Factory\Table\Image\ImageNoScaleTableFactory;
use Bitkorn\Images\Factory\Table\Image\ImageTableFactory;
use Bitkorn\Images\Factory\Table\Image\SliderTableFactory;
use Bitkorn\Images\Factory\Tablex\Image\ImageTablexFactory;
use Bitkorn\Images\Factory\View\Helper\Image\ImageEmptySrcFactory;
use Bitkorn\Images\Factory\View\Helper\Image\ImageScaleSelectFactory;
use Bitkorn\Images\Factory\View\Helper\Image\ImageSrcFactory;
use Bitkorn\Images\Factory\View\Helper\ImageNoScale\ImageNoScaleSrcFactory;
use Bitkorn\Images\Factory\View\Helper\Slider\SliderImageSrcFactory;
use Bitkorn\Images\Form\Image\ImageEditForm;
use Bitkorn\Images\Form\Image\ImageNoScaleForm;
use Bitkorn\Images\Form\Image\ImageUploadForm;
use Bitkorn\Images\Form\Slider\SliderEditForm;
use Bitkorn\Images\Form\Slider\SliderUploadForm;
use Bitkorn\Images\Service\Image\ImageService;
use Bitkorn\Images\Table\Image\ImageGroupTable;
use Bitkorn\Images\Table\Image\ImageNoScaleTable;
use Bitkorn\Images\Table\Image\ImageTable;
use Bitkorn\Images\Table\Image\SliderTable;
use Bitkorn\Images\Tablex\Image\ImageTablex;
use Bitkorn\Images\View\Helper\Image\ImageEmptySrc;
use Bitkorn\Images\View\Helper\Image\ImageScaleSelect;
use Bitkorn\Images\View\Helper\Image\ImageSrc;
use Bitkorn\Images\View\Helper\ImageNoScale\ImageNoScaleSrc;
use Bitkorn\Images\View\Helper\Slider\SliderImageSrc;
use Laminas\Router\Http\Segment;
use Laminas\Router\Http\Literal;

return [
    'router' => [
        'routes' => [
            /*
             * Images
             */
            'bitkornimages_admin_images' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bk-images/images',
                    'defaults' => [
                        'controller' => ImageAdminController::class,
                        'action' => 'images',
                    ],
                ],
            ],
            'bitkornimages_admin_uploadimage' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bk-images/upload-image',
                    'defaults' => [
                        'controller' => ImageAdminController::class,
                        'action' => 'uploadImage',
                    ],
                ],
            ],
            'bitkornimages_admin_editimage' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/bk-images/edit-image[/:image_id]',
                    'constraints' => [
                        'image_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ImageAdminController::class,
                        'action' => 'editImage',
                    ],
                ],
            ],
            /*
             * Slider
             */
            'bitkornimages_admin_sliderimages' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bk-images/slider-images',
                    'defaults' => [
                        'controller' => SliderImageAdminController::class,
                        'action' => 'sliderImages',
                    ],
                ],
            ],
            'bitkornimages_admin_uploadsliderimage' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bk-images/upload-slider-image',
                    'defaults' => [
                        'controller' => SliderImageAdminController::class,
                        'action' => 'uploadSliderImage',
                    ],
                ],
            ],
            'bitkornimages_admin_editsliderimage' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/bk-images/edit-slider-image[/:slider_image_id]',
                    'constraints' => [
                        'slider_image_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => SliderImageAdminController::class,
                        'action' => 'editSliderImage',
                    ],
                ],
            ],
            /*
             * No Scale Images
             */
            'bitkornimages_admin_imagesnoscale' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bk-images/images-noscale',
                    'defaults' => [
                        'controller' => ImageNoScaleAdminController::class,
                        'action' => 'imagesNoScale',
                    ],
                ],
            ],
            'bitkornimages_admin_uploadimagenoscale' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bk-images/upload-image-noscale',
                    'defaults' => [
                        'controller' => ImageNoScaleAdminController::class,
                        'action' => 'uploadImageNoScale',
                    ],
                ],
            ],
            'bitkornimages_admin_editimagenoscale' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/bk-images/edit-image-noscale[/:image_id]',
                    'constraints' => [
                        'image_id' => '[0-9]*'
                    ],
                    'defaults' => [
                        'controller' => ImageNoScaleAdminController::class,
                        'action' => 'editImageNoScale',
                    ],
                ],
            ],
            /*
             * Ajax Image Browsers
             */
            'bitkornimages_ajaxhelper_imagebrowser_imagebrowse' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/bk-images/imagebrowse[/:image_sizedef]',
                    'constraints' => [
                        'image_sizedef' => '[0-9a-z_]*',
                    ],
                    'defaults' => [
                        'controller' => ImageBrowserController::class,
                        'action' => 'imageBrowse',
                    ],
                ],
            ],
            'bitkornimages_ajaxhelper_imagebrowser_imagebrowsesize' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/bk-images/imagebrowsesize[/:image_sizedef]',
                    'constraints' => [
                        'image_sizedef' => '[0-9a-z_]*',
                    ],
                    'defaults' => [
                        'controller' => ImageBrowserController::class,
                        'action' => 'imageBrowseSize',
                    ],
                ],
            ],
            /*
             * Ajax Slider Image Browser
             */
            'bitkornimages_ajaxhelper_sliderimagebrowser_sliderimagebrowse' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/bk-images/slider-imagebrowse',
                    'defaults' => [
                        'controller' => SliderImageBrowserController::class,
                        'action' => 'sliderImageBrowse',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            ImageAdminController::class => ImageAdminControllerFactory::class,
            SliderImageAdminController::class => SliderImageAdminControllerFactory::class,
            ImageNoScaleAdminController::class => ImageNoScaleAdminControllerFactory::class,
            /*
             * ajax helper
             */
            ImageBrowserController::class => ImageBrowserControllerFactory::class,
            SliderImageBrowserController::class => SliderImageBrowserControllerFactory::class,
        ]
    ],
    'service_manager' => [
        'abstract_factories' => [
        ],
        'factories' => [
            ImageService::class => ImageServiceFactory::class,
            /*
             * table
             */
            ImageTable::class => ImageTableFactory::class,
            ImageGroupTable::class => ImageGroupTableFactory::class,
            SliderTable::class => SliderTableFactory::class,
            ImageNoScaleTable::class => ImageNoScaleTableFactory::class,
            ImageTablex::class => ImageTablexFactory::class,
            /*
             * form
             */
            ImageUploadForm::class => ImageUploadFormFactory::class,
            ImageEditForm::class => ImageEditFormFactory::class,
            ImageNoScaleForm::class => ImageNoScaleFormFactory::class,
            SliderUploadForm::class => SliderUploadFormFactory::class,
            SliderEditForm::class => SliderEditFormFactory::class,
        ],
        'invokables' => [
        ],
    ],
    'translator' => [
//        'locale' => 'de_DE', // BitkornListener->setupLocalization() kann das hier nicht überschreiben
        'translation_file_patterns' => [
            /*
             * Das hier kann auch in der Module.php->onBootstrap()
             * $this->translator->addTranslationFilePattern($type, $baseDir, $pattern)
             * 
             * zum kategorisieren kann man z.B.:
             * 'pattern'  => 'bitkorn_%s.mo'
             * ...%s ist der language code
             */
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
                'text_domain' => 'bitkornimages'
            ],
        ],
    ],
    'view_helpers' => [
        'factories' => [
            ImageSrc::class => ImageSrcFactory::class,
            ImageEmptySrc::class => ImageEmptySrcFactory::class,
            ImageScaleSelect::class => ImageScaleSelectFactory::class,
            SliderImageSrc::class => SliderImageSrcFactory::class,
            ImageNoScaleSrc::class => ImageNoScaleSrcFactory::class,
        ],
        'aliases' => [
            'imageSrc' => ImageSrc::class,
            'imageEmptySrc' => ImageEmptySrc::class,
            'imageScaleSelect' => ImageScaleSelect::class,
            'sliderImageSrc' => SliderImageSrc::class,
            'imageNoScaleSrc' => ImageNoScaleSrc::class,
        ],
        'invokables' => [
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'template/image/imageScaleSelect' => __DIR__ . '/../view/template/image/imageScaleSelect.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'view_helper_config' => [],
    'bitkorn_images' => [
        'image_path_absolute' => __DIR__ . '/../../../../public/img/bkimages',
        'image_path_relative' => '/img/bkimages',
        'slider_path_absolute' => __DIR__ . '/../../../../public/img/bkimages/slider',
        'slider_path_relative' => '/img/bkimages/slider',
        'image_path_absolute_no_scale' => __DIR__ . '/../../../../public/img/bkimages/noscale',
        'image_path_relative_no_scale' => '/img/bkimages/noscale',
        'slider_aspect_ratio' => '16:9', // UNUSED - Formel Seitenverhaeltnis: 1920 / 1080 = x / 1
        'slider_image_size' => '1920x816', // 1920x816 (Seitenverhaeltnis = 2,353 : 1)
    ]
];

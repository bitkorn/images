<?php

namespace Bitkorn\Images\Controller\Ajaxhelper\Admin;

use Bitkorn\Images\Table\Image\SliderTable;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\View\Model\ViewModel;

class SliderImageBrowserController extends AbstractUserController
{
    protected array $imagesConfig;
    protected SliderTable $sliderTable;

    public function setImagesConfig(array $imagesConfig): void
    {
        $this->imagesConfig = $imagesConfig;
    }

    public function setSliderTable(SliderTable $sliderTable)
    {
        $this->sliderTable = $sliderTable;
    }

    /**
     * @return ViewModel|Response
     */
    public function sliderImageBrowseAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();
        $this->layout('layout/nothing');

        $page = (int)$this->params()->fromQuery('page', 0);
        $itemsPerPage = (int)$this->params()->fromQuery('itemsperpage', 2000);
        $offset = $page * $itemsPerPage;

        $sliders = $this->sliderTable->getSliders();
        $viewModel->setVariable('sliders', $sliders);

        $viewModel->setVariable('sliderRelRoot', $this->imagesConfig['slider_path_relative'] . '/');

        return $viewModel;
    }

}

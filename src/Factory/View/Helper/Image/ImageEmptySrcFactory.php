<?php

namespace Bitkorn\Images\Factory\View\Helper\Image;

use Bitkorn\Images\View\Helper\Image\ImageEmptySrc;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ImageEmptySrcFactory implements FactoryInterface
{
	/**
	 * Create an object
	 *
	 * @param ContainerInterface $container
	 * @param string $requestedName
	 * @param null|array $options
	 * @return object
	 * @throws ServiceNotFoundException if unable to resolve the service
	 * @throws ServiceNotCreatedException if an exception is raised when creating a service
	 */
	public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
	{
		$viewHelper = new ImageEmptySrc();
		$viewHelper->setLogger($container->get('logger'));
        $viewHelper->setBitkornImagesConfig($container->get('config')['bitkorn_images']);
		return $viewHelper;
	}
}

<?php

namespace Bitkorn\Images\View\Helper\ImageNoScale;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;

/**
 *
 * @author Torsten Brieskorn
 */
class ImageNoScaleSrc extends AbstractViewHelper
{

    /**
     *
     * @var array
     */
    private $bitkornImagesConfig;

    /**
     * @param array $bitkornImagesConfig
     */
    public function setBitkornImagesConfig(array $bitkornImagesConfig)
    {
        $this->bitkornImagesConfig = $bitkornImagesConfig;
    }

    /**
     * @param array $image
     * @return string
     */
    public function __invoke(array $image)
    {
        if (empty($image)) {
            return '';
        }

        $imageYear = date('Y', $image['bk_images_image_noscale_time_create']);
        $imageMonth = date('m', $image['bk_images_image_noscale_time_create']);
        $imagePath = '/' . $imageYear . '/' . $imageMonth
            . '/' . $image['bk_images_image_noscale_id'] . '/' . $image['bk_images_image_noscale_filename'] . '.' . $image['bk_images_image_noscale_extension'];
        $imageSrcAbsolute = realpath($this->bitkornImagesConfig['image_path_absolute_no_scale']) . $imagePath;
        if (!file_exists($imageSrcAbsolute)) {
            throw new \RuntimeException('Image NoScale does not exist: ' . $imageSrcAbsolute);
        }
        return $this->bitkornImagesConfig['image_path_relative_no_scale'] . $imagePath;
    }
}

<?php

namespace Bitkorn\Images\Service\Image;

use Bitkorn\Images\Table\Image\ImageGroupTable;
use Bitkorn\Images\Table\Image\ImageNoScaleTable;
use Bitkorn\Images\Table\Image\ImageTable;
use Bitkorn\Images\Table\Image\SliderTable;
use Bitkorn\Images\Tools\Image\ImageScale;
use Bitkorn\Images\Tools\Image\ImageWatermark;
use Bitkorn\Trinket\Service\AbstractService;
use Bitkorn\Trinket\Tools\Filesystem\FilenameTool;
use Bitkorn\Trinket\Tools\Filesystem\FolderTool;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;

/**
 * ImageService load images by IDs.
 * Give his relative and absolute paths.
 * ...also for scaled images. Dummy image if a image not exist.
 *
 * @author allapow
 */
class ImageService extends AbstractService
{

    /**
     * @var array
     */
    protected $imagesConfig;

    /**
     *
     * @var ImageTable
     */
    protected $imageTable;

    /**
     *
     * @var ImageNoScaleTable
     */
    protected $imageNoScaleTable;

    /**
     *
     * @var ImageGroupTable
     */
    protected $imageGroupTable;

    /**
     * @var SliderTable
     */
    protected $sliderTable;

    /**
     * @param array $imagesConfig
     */
    public function setImagesConfig(array $imagesConfig): void
    {
        $this->imagesConfig = $imagesConfig;
    }

    /**
     * @param ImageTable $imageTable
     */
    public function setImageTable(ImageTable $imageTable): void
    {
        $this->imageTable = $imageTable;
    }

    /**
     * @param ImageNoScaleTable $imageNoScaleTable
     */
    public function setImageNoScaleTable(ImageNoScaleTable $imageNoScaleTable): void
    {
        $this->imageNoScaleTable = $imageNoScaleTable;
    }

    /**
     * @param ImageGroupTable $imageGroupTable
     */
    public function setImageGroupTable(ImageGroupTable $imageGroupTable): void
    {
        $this->imageGroupTable = $imageGroupTable;
    }

    /**
     * @param SliderTable $sliderTable
     */
    public function setSliderTable(SliderTable $sliderTable): void
    {
        $this->sliderTable = $sliderTable;
    }

    /**
     * @param array $storage
     * @param string $extension
     * @return int New image ID or -1 on error.
     */
    public function insertAndUploadImageWithStandardScalings(array $storage, string $extension): int
    {
        $currentTime = time();
        $success = true;
        $newImageId = $this->imageTable->saveImage([
            'bk_images_imagegroup_id' => $storage['bk_images_imagegroup_id'],
            'bk_images_image_title' => $storage['bk_images_image_title'],
            'bk_images_image_desc' => $storage['bk_images_image_desc'],
            'bk_images_image_desc_intern' => $storage['bk_images_image_desc_intern'],
            'bk_images_image_filename' => '',
            'bk_images_image_extension' => $extension,
            'bk_images_image_priority' => intval($storage['bk_images_image_priority']),
            'bk_images_image_scaling' => '',
            'bk_images_image_time_create' => $currentTime
        ]);
        $folder = FolderTool::computeDateIdFolder($this->imagesConfig['image_path_absolute'], date('Y', $currentTime), date('m', $currentTime), $newImageId);

        if ($newImageId > 0) {
            $filename = FilenameTool::computeSeoFriendlyFilename($storage['bk_images_image_title']);
            $filenameCompl = $filename . '.' . $extension;
            ImageWatermark::doWatermark($storage['bk_images_image_image']['tmp_name']);
            if (move_uploaded_file($storage['bk_images_image_image']['tmp_name'], $folder . '/' . $filenameCompl)) {
                $imageScaling = [];
                $imageScaling['original'] = 1;
                $imageScaling['magicTwoSquare'] = ImageScale::makeMagicTwoSquare($folder, $filename, $extension);
                $imageScaling['fullHd'] = ImageScale::makeFullHd($folder, $filename, $extension);
                $imageScaling['sixteenToNine'] = ImageScale::makeSixteenToNines($folder, $filename, $extension);
                $imageScaling['custom'] = [];
                $imageScaling['custom'][] = ImageScale::makeCustomScale($folder, $filename, $extension, ['width' => 200, 'height' => 200]); // preferred by facebook (share button)
                if ($this->imageTable->updateImage([
                        'bk_images_image_filename' => $filename,
                        'bk_images_image_scaling' => json_encode($imageScaling)
                    ], $newImageId) < 0) {
                    $success = false;
                }
            } else {
                $success = false;
            }
        } else {
            return false;
        }
        if (!$success) {
            $this->message = 'Es gab einen Fehler beim Speichern des Bildes';
            $this->deleteImage($newImageId);
            FolderTool::deleteDirRecursive($folder);
            return -1;
        }
        return $newImageId;
    }

    /**
     * @param array $storage
     * @param int $imageId
     * @return bool
     */
    public function updateImage(array $storage, int $imageId): bool
    {
        return $this->imageTable->updateImage($storage, $imageId) >= 0;
    }

    /**
     * @param int $imageId
     * @return bool
     */
    public function deleteImage(int $imageId): bool
    {
        return $this->imageTable->deleteImage($imageId);
    }

    /**
     * @param int $imageId
     * @return bool
     */
    public function deleteImageComplete(int $imageId): bool
    {
        if($imageId <= 0) {
            return false;
        }
        $image = $this->imageTable->getImageById($imageId);
        if(empty($image)) {
            return false;
        }
        $time = $image['bk_images_image_time_create'];
        FolderTool::deleteDirRecursive(FolderTool::computeDateIdFolder($this->imagesConfig['image_path_absolute'], date('Y', $time), date('m', $time), $imageId));
        return $this->imageTable->deleteImage($imageId);
    }
}

# BitkornImages
## Features
- upload & delete images
    - delete triggers an event to handle it in other modules
- reference images in other tables with an image-ID
- manage various scalings in upload and referencing
- ViewHelper for relative image URLs

## System Requirements
- chmod -R 775 /public/img/bkimages
- chown -R allapow:www-data /public/img/bkimages
- BitkornLib
- BitkornUser
- ImageMagic (http://de.php.net/imagick)

## Image Size Specification
### JSON ImageName Mapping
Examle: {"original":1,"magicTwoSquare":[1024,512,256,128,64,32,16],"fullHd":1,"sixteenToNine":[120,80,60,50,40,30,20,10,8,6,4,2,1],"custom":["200x200"]}

original            yes (1) | no (0)

magicTwoSquare      1024 = 1024 x 1024

fullHd              yes (1) | no (0)

sixteenToNine       the number ist the multiplicator for 16 or 9 (120 is 1920 x 1080 because 120 x 16 = 1920 and 120 x 9 = 1080)

custom              defines the custome size (200x200 mean 200 x 200 :))

#### Short Specification
- key without a value for 'original' and 'fullHd'
- key with value and underscore as glue (mts_1024 or stn_20 or custom_20x20)

#### Image File Naming Specification
1. image name
2. underscore
3. Size Specification: 
    - original: nothing
    - magicTwoSquare: mts_value (mts_1024)
    - fullHd: fullhd
    - sixteenToNine: stn_value (stn_120)
    - custom: value (200x200)
4. file extension

##### example for 'no_image'
no_image_200x200.png
no_image_fullhd.png
no_image_mts_1024.png
no_image_mts_128.png
no_image_mts_16.png
no_image_mts_256.png
no_image_mts_32.png
no_image_mts_512.png
no_image_mts_64.png
no_image.png
no_image_stn_10.png
no_image_stn_120.png
no_image_stn_1.png
no_image_stn_20.png
no_image_stn_2.png
no_image_stn_30.png
no_image_stn_40.png
no_image_stn_4.png
no_image_stn_50.png
no_image_stn_60.png
no_image_stn_6.png
no_image_stn_80.png
no_image_stn_8.png
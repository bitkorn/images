<?php

namespace Bitkorn\Images\Factory\Controller\Admin;

use Bitkorn\Images\Controller\Admin\ImageNoScaleAdminController;
use Bitkorn\Images\Form\Image\ImageNoScaleForm;
use Bitkorn\Images\Table\Image\ImageNoScaleTable;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ImageNoScaleAdminControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new ImageNoScaleAdminController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFolderTool($container->get(FolderTool::class));
        $controller->setImagesConfig($container->get('config')['bitkorn_images']);
        $controller->setImageNoScaleTable($container->get(ImageNoScaleTable::class));
        $controller->setImageNoScaleForm($container->get(ImageNoScaleForm::class));
        return $controller;
    }
}

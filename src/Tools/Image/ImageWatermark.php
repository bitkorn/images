<?php

namespace Bitkorn\Images\Tools\Image;

/**
 * Description of ImageWatermark
 *
 * @author allapow
 */
class ImageWatermark
{
    
    public static function doWatermark($fqFileName)
    {
        try {
            $image = new \Imagick($fqFileName);
            $imageGeo = $image->getimagegeometry();
            /**
             * @todo Bild muss dynamisch oder ein Branding-neutrales Bild
             */
            $watermarkFqFileName = realpath(__DIR__ . '/../../../data/watermark.png');
            $watermark = new \Imagick($watermarkFqFileName);
            $watermarkGeo = $watermark->getimagegeometry();
            $image->setimagegravity(\Imagick::GRAVITY_CENTER);
            $image->compositeimage($watermark, $watermark->getimagecompose(), $imageGeo['width'] / 2 - $watermarkGeo['width'] / 2, $imageGeo['height'] / 2 - $watermarkGeo['height'] / 2);
            $image->writeimage($fqFileName);
        } catch (\ImagickException $exception) {
            /**
             * @todo hier fehlt n Logger und/oder SimpleMailer
             */
        }
    }
}

<?php

namespace Bitkorn\Images\Controller\Admin;

use Bitkorn\Images\Form\Slider\SliderEditForm;
use Bitkorn\Images\Form\Slider\SliderUploadForm;
use Bitkorn\Images\Table\Image\SliderTable;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\Trinket\Tools\Filesystem\FilenameTool;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;
use Bitkorn\Images\Tools\Image\ImageScale;

class SliderImageAdminController extends AbstractUserController
{
    protected FolderTool $folderTool;
    protected array $imagesConfig;
    private SliderTable $sliderTable;
    private SliderUploadForm $sliderUploadForm;
    private SliderEditForm $sliderEditForm;

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    public function setImagesConfig(array $imagesConfig): void
    {
        $this->imagesConfig = $imagesConfig;
    }

    public function setSliderTable(SliderTable $sliderTable)
    {
        $this->sliderTable = $sliderTable;
    }

    public function setSliderUploadForm(SliderUploadForm $sliderUploadForm)
    {
        $this->sliderUploadForm = $sliderUploadForm;
    }

    public function setSliderEditForm(SliderEditForm $sliderEditForm)
    {
        $this->sliderEditForm = $sliderEditForm;
    }

    /**
     * @return ViewModel|Response
     */
    public function sliderImagesAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        $sliderImages = $this->sliderTable->getSliders();
        $viewModel->setVariable('sliderImages', $sliderImages);
        $viewModel->setVariable('sliderImagesRelPath', $this->imagesConfig['slider_path_relative']);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['sliderDelete']['sliderImageId'])) {
                $jsonModel = new JsonModel();
                $deleteStatus = 0;
                $sliderImageId = (int)$postData['sliderDelete']['sliderImageId'];
                $sliderData = $this->sliderTable->getSliderById($sliderImageId);
                if (!empty($sliderData) && $this->sliderTable->delete(['bk_images_slider_id' => $sliderImageId])) {
                    $imageTimeCreate = $sliderData['bk_images_slider_time_create'];
                    if (!empty($imageTimeCreate)) {
                        $folder = $this->folderTool->computeDateIdFolder($this->imagesConfig['slider_path_absolute'],
                            date('Y', $imageTimeCreate), date('m', $imageTimeCreate), $sliderImageId);

                        if ($this->folderTool->deleteDirRecursive($folder) === true) {
                            $deleteStatus = 1;
                        }
                    }
                }
                $jsonModel->setVariable('status', $deleteStatus);
                return $jsonModel;
            }
        }

        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function uploadSliderImageAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        $this->sliderUploadForm->init();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            ini_set('upload_max_filesize', '100M');
            ini_set('post_max_size', '100M');
            $postData = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            $this->sliderUploadForm->setData($postData);
            if ($this->sliderUploadForm->isValid()) {
                $formData = $this->sliderUploadForm->getData();
                $fileinfo = new \finfo(FILEINFO_MIME);
                $extension01 = explode(';', $fileinfo->file($formData['bk_images_slider_image']['tmp_name']));
                $extension02 = explode('/', $extension01[0]);
                $extension = $extension02[1];
                if (empty($extension)) {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text'  => 'Die Datei ist defekt. Bitte benutze eine andere oder repariere diese.'
                    ];
                } else {
                    $currentTime = time();
                    $newImageId = $this->sliderTable->saveSlider([
                        'bk_images_slider_title'          => $formData['bk_images_slider_title'],
                        'bk_images_slider_desc'           => $formData['bk_images_slider_desc'],
                        'bk_images_slider_filename'       => 'original',
                        'bk_images_slider_filename_thumb' => 'original',
                        'bk_images_slider_extension'      => $extension,
                        'bk_images_slider_time_create'    => $currentTime
                    ]);
                    if ($newImageId > 0) {
                        $folder = $this->folderTool->computeDateIdFolder($this->imagesConfig['slider_path_absolute'], date('Y', $currentTime),
                            date('m', $currentTime), $newImageId);
                        $filename = FilenameTool::computeSeoFriendlyFilename($formData['bk_images_slider_title']);
                        $filenameCompl = $filename . '.' . $extension;
//                        \Bitkorn\Images\Tools\Image\ImageWatermark::doWatermark($formData['bk_images_image_image']['tmp_name']);
                        if (move_uploaded_file($formData['bk_images_slider_image']['tmp_name'], $folder . '/' . $filenameCompl)) {
                            $sliderImageSize = explode('x', $this->imagesConfig['slider_image_size']);
                            $imageScaling = ImageScale::makeCustomScale($folder, $filename, $extension,
                                ['width' => $sliderImageSize[0], 'height' => $sliderImageSize[1]]);
                            $imageThumb = ImageScale::makeCustomHightScale($folder, $filename . '_' . $imageScaling, 70, $extension);
                            if ($this->sliderTable->updateSliderFilename($filename . '_' . $imageScaling, $filename . '_' . $imageScaling . '_' . $imageThumb,
                                    $newImageId) < 0) {
                                $this->layout()->message = [
                                    'level' => 'error',
                                    'text'  => 'Es gab einen Fehler mit der Datenbank.'
                                ];
                                $this->sliderTable->delete(['bk_images_slider_id' => $newImageId]);
                                $this->folderTool->deleteDirRecursive($folder);
                            } else {
                                $this->layout()->message = [
                                    'level' => 'success',
                                    'text'  => 'Das Bild wurde mit seinen Daten gespeichert.'
                                ];
                            }
                        } else {
                            $this->sliderTable->delete(['bk_images_slider_id' => $newImageId]);
                            $this->layout()->message = [
                                'level' => 'error',
                                'text'  => 'Es gab einen Fehler mit dem Dateisystem.'
                            ];
                        }
                        $this->sliderUploadForm->init();
                    } else {
                        $this->layout()->message = [
                            'level' => 'error',
                            'text'  => 'Es gab einen Fehler mit der Datenbank.'
                        ];
                    }
                }
            }
        }

        $viewModel->setVariable('sliderForm', $this->sliderUploadForm);

        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function editSliderImageAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $sliderImageId = $this->params('slider_image_id');
        $sliderImageData = $this->sliderTable->getSliderById($sliderImageId);
        if (empty($sliderImageData)) {
            return $this->redirect()->toRoute('bitkornimages_admin_sliderimages');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('sliderImageId', $sliderImageId);
        $this->sliderEditForm->init();
        $this->sliderEditForm->setData($sliderImageData);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $this->sliderEditForm->setData($postData);
            if ($this->sliderEditForm->isValid()) {
                $formData = $this->sliderEditForm->getData();
                unset($formData['submit']);
                if ($this->sliderTable->updateSliderData($sliderImageId, $formData) >= 0) {
                    $this->layout()->message = [
                        'level' => 'success',
                        'text'  => 'Die Daten wurden gespeichert.'
                    ];
                } else {
                    $this->layout()->message = [
                        'level' => 'error',
                        'text'  => 'Es gab einen Fehler mit der Datenbank.'
                    ];
                }
            }
        }

        $viewModel->setVariable('sliderEditForm', $this->sliderEditForm);

        $this->layout('layout/admin');
        return $viewModel;
    }
}

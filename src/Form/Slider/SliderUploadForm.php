<?php

namespace Bitkorn\Images\Form\Slider;

use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class SliderUploadForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface {

	function __construct($name = 'bk_images_slider') {
		parent::__construct($name);
	}

	public function init() {
		$this->setAttribute('enctype', 'multipart/form-data');

		$imageTitle = new \Laminas\Form\Element\Text('bk_images_slider_title');
		$imageTitle->setLabel('Slider Titel');
		$imageTitle->setAttributes([
			'class' => 'w3-input',
			'title' => 'hieraus wird der SEO-freundliche Dateiname generiert!!!'
		]);
		$this->add($imageTitle);

		$imageDesc = new \Laminas\Form\Element\Textarea('bk_images_slider_desc');
		$imageDesc->setLabel('Beschreibung');
		$imageDesc->setAttributes([
			'class' => 'w3-input w3-border',
			'title' => 'for internal reason'
		]);
		$this->add($imageDesc);

		$this->add(array(
			'name' => 'bk_images_slider_filename',
			'attributes' => array(
				'type' => 'hidden',
			),
		));

		$image = new \Laminas\Form\Element\File('bk_images_slider_image');
		$image->setAttributes([
			'class' => 'w3-input w3-border',
			'accept' => 'image/jpg,image/jpeg,image/png'
		]);
		$this->add($image);

		$submit = new \Laminas\Form\Element\Submit('submit');
		$submit->setValue('upload & speichern');
		$submit->setAttributes([
			'class' => 'w3-input',
		]);
		$this->add($submit);

		return parent::init();
	}

	public function getInputFilterSpecification() {
		return array(
			'bk_images_slider_title' => array(
				'required' => true,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 200,
						),
					),
				),
			),
			'bk_images_slider_desc' => array(
				'required' => false,
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 32000,
						),
					),
				),
			),
			'bk_images_slider_filename' => array(
				'required' => false, // generiert aus shop_article_image_title
				'filters' => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name' => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min' => 1,
							'max' => 200,
						),
					),
				),
			),
			'bk_images_slider_image' => array(
				'required' => true,
				'filters' => array(
				),
				'validators' => array(
					[
						'name' => 'filemimetype',
						'break_chain_on_failure' => TRUE,
						'options' => [
							'mimeType' => 'image/jpeg, image/png',
							'magicFile' => FALSE, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
							'messages' => array(
								\Laminas\Validator\File\MimeType::FALSE_TYPE => "Falsches Dateiformat '%type%', nur jpeg/jpg | image/png!",
							),
						]
					],
					['name' => 'FileSize',
						'options' => [
							'max' => 10000000
						]
					],
					[
						'name' => 'fileimagesize',
						'options' => array(
							'maxWidth' => 160000, // 1600, 1440, 1280, 1120, 960
							'maxHeight' => 160000, // 900, 810, 720, 630, 540
							'messages' => array(
								\Laminas\Validator\File\ImageSize::HEIGHT_TOO_BIG => "Das Bild ist mit %height% zu hoch, max=%maxheight%!",
								\Laminas\Validator\File\ImageSize::WIDTH_TOO_BIG => "Das Bild ist mit %width% zu breit, max=%maxwidth%!",
							),
						),
					],
				),
			),
		);
	}

}

?>

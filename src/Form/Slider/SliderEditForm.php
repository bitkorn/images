<?php

namespace Bitkorn\Images\Form\Slider;

use Laminas\Form\Form;

/**
 *
 * @author allapow
 */
class SliderEditForm extends Form implements \Laminas\InputFilter\InputFilterProviderInterface
{

    function __construct($name = 'bk_images_slider')
    {
        parent::__construct($name);
    }

    public function init()
    {

        $this->add(array(
            'name' => 'bk_images_slider_id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));

        $imageTitle = new \Laminas\Form\Element\Text('bk_images_slider_title');
        $imageTitle->setLabel('Slider Titel');
        $imageTitle->setAttributes([
            'class' => 'w3-input',
            'title' => 'hieraus wird der SEO-freundliche Dateiname generiert!!!'
        ]);
        $this->add($imageTitle);

        $imageDesc = new \Laminas\Form\Element\Textarea('bk_images_slider_desc');
        $imageDesc->setLabel('Beschreibung');
        $imageDesc->setAttributes([
            'class' => 'w3-input w3-border',
            'title' => 'for internal reason'
        ]);
        $this->add($imageDesc);

        $submit = new \Laminas\Form\Element\Submit('submit');
        $submit->setValue('speichern');
        $submit->setAttributes([
            'class' => 'w3-input',
        ]);
        $this->add($submit);

        return parent::init();
    }

    public function getInputFilterSpecification()
    {
        return array(
            'bk_images_slider_id' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'Digits',
                    ),
                ),
            ),
            'bk_images_slider_title' => array(
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 200,
                        ),
                    ),
                ),
            ),
            'bk_images_slider_desc' => array(
                'required' => false,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min' => 1,
                            'max' => 32000,
                        ),
                    ),
                ),
            ),
        );
    }

}

?>

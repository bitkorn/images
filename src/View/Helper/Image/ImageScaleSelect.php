<?php

namespace Bitkorn\Images\View\Helper\Image;

use Bitkorn\Trinket\View\Helper\AbstractViewHelper;
use Laminas\View\Model\ViewModel;

/**
 *
 * @author allapow
 */
class ImageScaleSelect extends AbstractViewHelper
{

    const TEMPLATE = 'template/image/imageScaleSelect';

    /**
     *
     * @var array
     */
    private $bitkornImagesConfig;

    /**
     *
     * @param array $bitkornImagesConfig
     */
    public function setBitkornImagesConfig(array $bitkornImagesConfig)
    {
        $this->bitkornImagesConfig = $bitkornImagesConfig;
    }

    private function getOptionHtml($imgSrc, $optionText)
    {
        return '<option value="' . $imgSrc . '">' . $optionText . '</option>';
    }

    /**
     * @param array $image
     * @return string|void
     */
    public function __invoke(array $image)
    {
        if (empty($image) || empty($image['bk_images_image_id'])) {
            return '';
        }
        $viewModel = new ViewModel();
        $viewModel->setTemplate(self::TEMPLATE);

        $viewModel->setVariable('imageId', $image['bk_images_image_id']);

        $imageScalings = json_decode($image['bk_images_image_scaling'], true);
        if (!$imageScalings) {
            return;
        }
        $viewModel->setVariable('imageScalings', $imageScalings);

        $relFileName = $this->bitkornImagesConfig['image_path_relative'] . '/' . date('Y') . '/' . date('m') . '/' . $image['bk_images_image_id']
            . '/' . $image['bk_images_image_filename'] . '_SCALING.' . $image['bk_images_image_extension'];
        $viewModel->setVariable('relFileName', $relFileName);

        return $this->getView()->render($viewModel);
    }

}

-- MySQL dump 10.13  Distrib 5.6.33, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: systemgurt
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bk_images_image`
--

DROP TABLE IF EXISTS `bk_images_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bk_images_image` (
  `bk_images_image_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bk_images_imagegroup_id` int(10) unsigned NOT NULL,
  `bk_images_image_title` tinytext,
  `bk_images_image_desc` text,
  `bk_images_image_filename` tinytext NOT NULL,
  `bk_images_image_extension` varchar(10) NOT NULL,
  `bk_images_image_priority` int(11) NOT NULL DEFAULT '1',
  `bk_images_image_scaling` text NOT NULL COMMENT 'JSON {"magicTwoSquare":[1024,32,16],"fullHd":1,"sixteenToNine":[60,50,40,30,20,10,8,6],"custom":["42x27"]}',
  `bk_images_image_time_create` int(10) unsigned NOT NULL,
  PRIMARY KEY (`bk_images_image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bk_images_image`
--

LOCK TABLES `bk_images_image` WRITE;
/*!40000 ALTER TABLE `bk_images_image` DISABLE KEYS */;
INSERT INTO `bk_images_image` VALUES (15,0,'Bahnhofshinterfahrung','Straßenkarte der Bahnhofshinterfahrung','bahnhofshinterfahrung','png',23,'{\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"27x42\"]}',1489912689),(16,0,'Leo Klassenfoto','','leo_klassenfoto','jpeg',0,'{\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"27x42\"]}',1489912785),(17,0,'dear aliens','','dear_aliens','jpeg',0,'{\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"27x42\"]}',1489915259),(18,0,'alte Festplatte','Eine alte Festplatte unter einer Glasplatte','alte_festplatte','jpeg',3,'{\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"27x42\"]}',1489915301),(19,1,'Heiss draußen','Cartoon: heiß draußen','heiss_draussen','jpeg',0,'{\"magicTwoSquare\":[1024,512,256,128,64,32,16],\"fullHd\":1,\"sixteenToNine\":[120,80,60,50,40,30,20,10,8,6,4,2,1],\"custom\":[\"27x42\"]}',1489916799);
/*!40000 ALTER TABLE `bk_images_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bk_images_imagegroup`
--

DROP TABLE IF EXISTS `bk_images_imagegroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bk_images_imagegroup` (
  `bk_images_imagegroup_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bk_images_imagegroup_name` varchar(100) NOT NULL,
  `bk_images_imagegroup_priority` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`bk_images_imagegroup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bk_images_imagegroup`
--

LOCK TABLES `bk_images_imagegroup` WRITE;
/*!40000 ALTER TABLE `bk_images_imagegroup` DISABLE KEYS */;
INSERT INTO `bk_images_imagegroup` VALUES (1,'cms',1),(2,'shop article',1),(3,'shop',1);
/*!40000 ALTER TABLE `bk_images_imagegroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'systemgurt'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-19 11:03:06

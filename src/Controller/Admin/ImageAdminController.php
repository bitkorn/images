<?php

namespace Bitkorn\Images\Controller\Admin;

use Bitkorn\Images\Entity\Image\ImagesEntity;
use Bitkorn\Images\Form\Image\ImageEditForm;
use Bitkorn\Images\Form\Image\ImageUploadForm;
use Bitkorn\Images\Service\Image\ImageService;
use Bitkorn\Images\Table\Image\ImageTable;
use Bitkorn\Images\Tablex\Image\ImageTablex;
use Bitkorn\Trinket\Observer\ObserverManager;
use Bitkorn\Trinket\Service\Filesystem\FolderTool;
use Bitkorn\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

class ImageAdminController extends AbstractUserController
{
    protected FolderTool $folderTool;
    protected array $imagesConfig;
    protected ImageTable $imageTable;
    protected ImageService $imageService;
    protected ImageTablex $imageTablex;
    protected ImageUploadForm $imageUploadForm;
    protected ImageEditForm $imageEditForm;

    public function setFolderTool(FolderTool $folderTool): void
    {
        $this->folderTool = $folderTool;
    }

    public function setImagesConfig(array $imagesConfig): void
    {
        $this->imagesConfig = $imagesConfig;
    }

    public function setImageTable(ImageTable $imageTable)
    {
        $this->imageTable = $imageTable;
    }

    public function setImageService(ImageService $imageService): void
    {
        $this->imageService = $imageService;
    }

    public function setImageTablex(ImageTablex $imageTablex)
    {
        $this->imageTablex = $imageTablex;
    }

    public function setImageUploadForm(ImageUploadForm $imageUploadForm)
    {
        $this->imageUploadForm = $imageUploadForm;
    }

    public function setImageEditForm(ImageEditForm $imageEditForm)
    {
        $this->imageEditForm = $imageEditForm;
    }

    /**
     * @return ViewModel|Response
     */
    public function imagesAction(): Response|ViewModel
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        $images = $this->imageTablex->getImages();
        $viewModel->setVariable('images', $images);
        $imageEntity = new ImagesEntity();
        $imageEntity->exchangeArrayImages($images);
        $viewModel->setVariable('imageEntity', $imageEntity);

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            if (!empty($postData['imageDelete']['imageId'])) {
                $jsonModel = new JsonModel();
                $deleteStatus = 0;
                $imageId = (int)$postData['imageDelete']['imageId'];
                ObserverManager::getInstance()->trigger('bitkornimages.image.delete.before', ['imageId' => $imageId]);
                if ($this->imageTable->delete(['bk_images_image_id' => $imageId])) {
                    $imageTimeCreate = $imageEntity->getImageTimeCreate($imageId);
                    if (!empty($imageTimeCreate)) {
                        $folder = $this->folderTool->computeDateIdFolder($this->imagesConfig['image_path_absolute'],
                            date('Y', $imageTimeCreate), date('m', $imageTimeCreate), $imageId);

                        if ($this->folderTool->deleteDirRecursive($folder) === true) {
                            $deleteStatus = 1;
                        }
                    }
                }
                $jsonModel->setVariable('status', $deleteStatus);
                return $jsonModel;
            }
        }

        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function uploadImageAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $viewModel = new ViewModel();

        $this->imageUploadForm->init();

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = array_merge_recursive($request->getPost()->toArray(), $request->getFiles()->toArray());
            $this->imageUploadForm->setData($postData);
            if ($this->imageUploadForm->isValid()) {
                $formData = $this->imageUploadForm->getData();
                $fileinfo = new \finfo(FILEINFO_MIME);
                $extension01 = explode(';', $fileinfo->file($formData['bk_images_image_image']['tmp_name']));
                $extension02 = explode('/', $extension01[0]);
                $extension = $extension02[1];
                if (empty($extension)) {
                    $this->layout()->message = [
                        'level' => 'warn',
                        'text'  => 'Die Datei ist defekt. Bitte benutze eine andere oder repariere diese.'
                    ];
                } else {
                    if ($this->imageService->insertAndUploadImageWithStandardScalings($formData, $extension) < 1) {
                        $this->layout()->message = [
                            'level' => 'error',
                            'text'  => $this->imageService->getMessage()
                        ];
                    } else {
                        $this->layout()->message = [
                            'level' => 'success',
                            'text'  => 'Der Upload & die Datenspeicherung war erfolgreich.'
                        ];
                    }
                    $this->imageUploadForm->init();
                }
            }
        }

        $viewModel->setVariable('imageForm', $this->imageUploadForm);

        $this->layout('layout/admin');
        return $viewModel;
    }

    /**
     * @return ViewModel|Response
     */
    public function editImageAction(): ViewModel|Response
    {
        if (!$this->userService->checkUserRoleAccessMin(3)) {
            return $this->redirect()->toRoute('bitkorn_user_html_authentication_login');
        }
        $imageId = $this->params('image_id');
        $imageData = $this->imageTable->getImageById($imageId);
        if (empty($imageData)) {
            return $this->redirect()->toRoute('bitkornimages_admin_images');
        }
        $viewModel = new ViewModel();
        $viewModel->setVariable('imageId', $imageId);
        $this->imageEditForm->init();
        $this->imageEditForm->setData($imageData);

        $forwardUrl = $this->params()->fromQuery('forwardurl');
        if (!empty($forwardUrl)) {
            // back & forward url
            $viewModel->setVariable('forwardUrl', $forwardUrl);
        }

        $request = $this->getRequest();
        if ($request->isPost() && $request instanceof Request) {
            $postData = $request->getPost()->toArray();
            $this->imageEditForm->setData($postData);
            if ($this->imageEditForm->isValid()) {
                $formData = $this->imageEditForm->getData();
                unset($formData['submit']);
                if ($this->imageTable->updateImage($formData, $imageId) >= 0) {
                    if (!empty($forwardUrl)) {
                        return $this->redirect()->toUrl($forwardUrl);
                    }
                    $this->layout()->message = [
                        'level' => 'success',
                        'text'  => 'Die Daten wurden gespeichert.'
                    ];
                } else {
                    $this->layout()->message = [
                        'level' => 'error',
                        'text'  => 'Es gab einen Fehler mit der Datenbank.'
                    ];
                }
            }
        }

        $viewModel->setVariable('imageEditForm', $this->imageEditForm);

        $this->layout('layout/admin');
        return $viewModel;
    }

}

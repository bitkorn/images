<?php

namespace Bitkorn\Images\Tablex\Image;

use Bitkorn\Images\Tablex\AbstractTablex;
use Laminas\Db\Adapter\ParameterContainer;

/**
 *
 * @author allapow
 */
class ImageTablex extends AbstractTablex
{

    /**
     *
     */
    const SELECT_JOIN_IMAGES = 'SELECT
                                        ii.*,
                                        iig.*
                                FROM
                                        bk_images_image ii
                                LEFT JOIN bk_images_imagegroup iig
                                                USING(bk_images_imagegroup_id)
                                ORDER BY
                                        iig.bk_images_imagegroup_priority DESC,
                                        ii.bk_images_image_priority DESC';

    /**
     *
     * @return array
     */
    public function getImages()
    {
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_IMAGES);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

    /**
     *
     */
    const SELECT_JOIN_IMAGES_FILTERED = '';

    /**
     *
     * @param int $imageGroupId
     * @return array
     */
    public function getImagesFiltered($imageGroupId)
    {
        $parameter = new ParameterContainer([$imageGroupId]);
        $stmt = $this->adapter->createStatement(self::SELECT_JOIN_IMAGES_FILTERED, $parameter);
        $result = $stmt->execute();
        $resultArr = [];
        if ($result->valid() && $result->count() > 0) {
            do {
                $current = $result->current();
                $resultArr[] = $current;
                $result->next();
            } while ($result->valid());
        }
        return $resultArr;
    }

}
